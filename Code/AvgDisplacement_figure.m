%% Avg. Displacement 
SelectKeys = {'NoCorrection', 'V2V_Simulated','MSV_Simulated','MT_Simulated'};
Labels = {'NoCor', 'V2V', 'S2V', 'HMT'};

k=1:1820; % All
Errors = zeros(numel(k), numel(SelectKeys));
for mtd = 1:numel(SelectKeys)
    result_folder = ResultMap(SelectKeys{mtd});
    load([result_folder 'PInfo.mat']);
    load([PInfo.dataset_path 'TruthParsGlobal.mat']);
    load([result_folder 'xt_map.mat']);
    Vol2SlcInfo = PInfo.vol2slc_info;
    Vol2SlcInfo.jmax = 14;
    Vol2SlcInfo.slice_sequence = PInfo.slice_sequence;
    AvgDp = ShowAvgDisplacement(k, xt_map, xt_global, Vol2SlcInfo); % Should not be zero
    Errors(:,mtd) = AvgDp(:);
end

meanError = mean(Errors);
figure;
boxplot(Errors, 'notch', 'on', 'label', Labels),...
    xlabel('Methods'),...
    ylabel('$D_t$', 'Interpreter', 'latex'),...
    axis([0 4.5 0 15]),...
    text(0,16, 'Mean:', 'HorizontalAlign', 'Left'),...
    text(1,16, num2str(meanError(1)), 'HorizontalAlign', 'Center'),...
    text(2,16, num2str(meanError(2)), 'HorizontalAlign', 'Center'),...
    text(3,16, num2str(meanError(3)), 'HorizontalAlign', 'Center'),...
    text(4,16, num2str(meanError(4)), 'HorizontalAlign', 'Center', 'Color', [1 0 0], 'FontWeight', 'bold');

