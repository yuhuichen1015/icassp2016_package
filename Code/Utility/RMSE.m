function [total, alpha, beta, gamma, xest, yest, zest]=RMSE(k, xt_map, xt_truth, draw)
    if(draw>0)        
        total=0; alpha=0; beta=0; gamma=0;
        if(draw==2)
            figure;
            plot(k, xt_map(1,k), 'r', k, xt_map(2,k), 'g', k, xt_map(3,k), 'b');
            hold on;
            plot(k, xt_truth(1,k), '-.r', k, xt_truth(2,k), '-.g', k, xt_truth(3,k), '-.b');
            alpha = sqrt(norm(xt_map(1,k)-xt_truth(1,k))^2/size(k,2))
            beta = sqrt(norm(xt_map(2,k)-xt_truth(2,k))^2/size(k,2))
            gamma = sqrt(norm(xt_map(3,k)-xt_truth(3,k))^2/size(k,2))
            legend('\alpha', '\beta', '\gamma');
            figure;
            plot(k, xt_map(4,k), 'r', k, xt_map(5,k), 'g', k, xt_map(6,k), 'b');
            hold on;
            plot(k, xt_truth(4,k), '-.r', k, xt_truth(5,k), '-.g', k, xt_truth(6,k), '-.b');
            xest = sqrt(norm(xt_map(4,k)-xt_truth(4,k))^2/size(k,2))
            yest = sqrt(norm(xt_map(5,k)-xt_truth(5,k))^2/size(k,2))
            zest = sqrt(norm(xt_map(6,k)-xt_truth(6,k))^2/size(k,2))
%             total = sqrt((alpha^2+beta^2+gamma^2+xest^2+yest^2+zest^2)/6)
            total = sqrt((alpha^2+beta^2+gamma^2)/3)
            legend('\delta_x', '\delta_y', '\delta_z');
        else
            figure;
            plot(k, xt_map(1,k), 'r', k, xt_map(2,k), 'g', k, xt_map(3,k), 'b');
            hold on;
            
            figure;
            plot(k, xt_map(4,k), 'r', k, xt_map(5,k), 'g', k, xt_map(6,k), 'b');
            hold on;
        end
    else
        alpha = sqrt(norm(xt_map(1,k)-xt_truth(1,k))^2/size(k,2))
        beta = sqrt(norm(xt_map(2,k)-xt_truth(2,k))^2/size(k,2))
        gamma = sqrt(norm(xt_map(3,k)-xt_truth(3,k))^2/size(k,2))
        xest = sqrt(norm(xt_map(4,k)-xt_truth(4,k))^2/size(k,2))
        yest = sqrt(norm(xt_map(5,k)-xt_truth(5,k))^2/size(k,2))
        zest = sqrt(norm(xt_map(6,k)-xt_truth(6,k))^2/size(k,2))
        total = sqrt((alpha^2+beta^2+gamma^2+xest^2+yest^2+zest^2)/6)
        
        Num_of_slices = 14;
        RMSE_Slice = zeros(Num_of_slices,1);
        for i=1:Num_of_slices
            kk = mod(k, Num_of_slices);
            if(i~=Num_of_slices)
                idxk = k(kk==i);                
            else                
                idxk = k(kk==0);  
            end
            alpha = sqrt(norm(xt_map(1,idxk)-xt_truth(1,idxk))^2/size(idxk,2));
            beta = sqrt(norm(xt_map(2,idxk)-xt_truth(2,idxk))^2/size(idxk,2));
            gamma = sqrt(norm(xt_map(3,idxk)-xt_truth(3,idxk))^2/size(idxk,2));
            xest = sqrt(norm(xt_map(4,idxk)-xt_truth(4,idxk))^2/size(idxk,2));
            yest = sqrt(norm(xt_map(5,idxk)-xt_truth(5,idxk))^2/size(idxk,2));
            zest = sqrt(norm(xt_map(6,idxk)-xt_truth(6,idxk))^2/size(idxk,2));
            RMSE_Slice(i) = sqrt((alpha^2+beta^2+gamma^2+xest^2+yest^2+zest^2)/6);
        end
        figure;
        plot(RMSE_Slice), axis([1 Num_of_slices 0 1]);
    end
end