run('../Initial.m');
TEST_BEGIN;
%%

clear;

min_slices = [0 0 0];
max_slices = [200 200 100];

%%%
pars = [0;30;0;0;0;0]
[PtsMtx] = Pars2CtrlPts(pars, min_slices, max_slices);
[Pars]=CtrlPts2Pars(PtsMtx)


%%%
pars = [0;0;0;5;10;20]
[PtsMtx] = Pars2CtrlPts(pars, min_slices, max_slices);
[Pars]=CtrlPts2Pars(PtsMtx)


%%%
for trial = 1:10000
    pars = 15*randn(6,1)
    [PtsMtx] = Pars2CtrlPts(pars, min_slices, max_slices);
    [Pars]=CtrlPts2Pars(PtsMtx);
    if(sum(pars(:)-Pars(:))>0.001)
        pars
        Pars
        error('Incorrect recovery');
        break;
    end
end


%%
TEST_END;
