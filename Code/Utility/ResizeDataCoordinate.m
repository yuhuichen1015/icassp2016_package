function [min_update, max_update] = ResizeDataCoordinate(dim, min_ext, max_ext, dim_target)
    if(size(dim(:),1)~=size(min_ext(:),1) ||...
            size(dim(:),1)~=size(max_ext(:),1) ||...
            size(dim(:),1)~=size(dim_target(:),1))
        error('The sizes of the inputs should be all the same.');
    else
        Sz = size(dim(:),1);
    end
    
    if(any(max_ext-min_ext<0) || any(dim<3) || any(dim_target<3))
        dim
        min_ext
        max_ext
        error('Wrong input data');
    end
    min_update = zeros(size(min_ext));
    max_update = zeros(size(max_ext));
    for i=1:Sz
        line_coordinate = linspace(min_ext(i), max_ext(i), dim(i));
        vollength = line_coordinate(2)-line_coordinate(1);
        min_cor = line_coordinate(1)-vollength/2;
        max_cor = line_coordinate(end)+vollength/2;
        newvollength = (max_cor-min_cor)/(dim_target(i));
        min_update(i) = min_cor + newvollength/2;
        max_update(i) = max_cor - newvollength/2;
    end
end