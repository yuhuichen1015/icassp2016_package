function [AvgDp]=CalcAvgDisplacement(xt_1, xt_2, slc_idx, DataInfo)
% xt_truth must be in global view.

% check field existance
if(~isfield(DataInfo, 'dim') || ~isfield(DataInfo, 'min_ext') ||...
        ~isfield(DataInfo, 'max_ext'))
    error('DataInfo must contains refernce vol coordinate info.');
else
    dim = DataInfo.dim;
    min_ext = DataInfo.min_ext;
    max_ext = DataInfo.max_ext;
end
if(~isfield(DataInfo, 'dim_slices') || ~isfield(DataInfo, 'min_ext_slices') ||...
        ~isfield(DataInfo, 'max_ext_slices'))
    error('DataInfo must contains slice coordinate info');
else
    dim_slices = DataInfo.dim_slices;
    min_ext_slices = DataInfo.min_ext_slices;
    max_ext_slices = DataInfo.max_ext_slices;
end

if(~isfield(DataInfo, 'joint_origin'))
    joint_origin = zeros(3,1);
else
    joint_origin = DataInfo.joint_origin;
end

% Check the system offset information
if(~isfield(DataInfo, 'sysoffset_angles'))
    sysoffset_xyz = zeros(3,1);
    Asys = eye(3);
else
    sysoffset_xyz = DataInfo.sysoffset_xyz;
    Asys = DataInfo.sysoffset_dcm;
end

% Check whether the precalculated information exists
if(~isfield(DataInfo, 'delta1'))
    % Prepare the transformed slice coordinate
    DataInfo = CalcCoordinateInfo(DataInfo);
end
line_1 = DataInfo.line_1;
line_2 = DataInfo.line_2;
slice_Z_values = DataInfo.slice_z_values;

% Compute the coordinate for each grid.
z_cor = slice_Z_values(slc_idx)*ones([dim_slices(1)*dim_slices(2),1]);
xyz_cor = [line_1(:)'; line_2(:)'; z_cor(:)'];
GridCoordinates = Asys*xyz_cor + repmat(sysoffset_xyz(:), [1, size(xyz_cor,2)]) - ...
    repmat(joint_origin(:), [1, size(xyz_cor,2)]);

% Prepare the rotation matrix
A_1 = EA2DCM(xt_1(1:3));
xyz_1 = xt_1(4:6);

A_2 = EA2DCM(xt_2(1:3));
xyz_2 = xt_2(4:6);

% Calculate the voxel displacement
rotated_1 = A_1*GridCoordinates +...
    repmat(xyz_1(:),[1 size(GridCoordinates,2)]);

rotated_2 = A_2*GridCoordinates +...
    repmat(xyz_2(:),[1 size(GridCoordinates,2)]);

AvgDp = mean(sqrt(sum((rotated_1-rotated_2).^2,1)));

end