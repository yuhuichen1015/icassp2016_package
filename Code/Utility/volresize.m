function [volRZ, DataInfoRZ] = volresize(vol, dim_target, DataInfo)
   
% Resize the volume to the target size and update the coordinates in
% datainfo
if(~isfield(DataInfo, 'dim') ||...
        ~isfield(DataInfo, 'min_ext') ||...
        ~isfield(DataInfo, 'max_ext'))
    error('DataInfo must contains vol coordinate info.');
else
    dim = DataInfo.dim;
    min_ext = DataInfo.min_ext;
    max_ext = DataInfo.max_ext;
end
if(size(dim_target(:),1)~=size(dim(:),1))
    error('The dimension size should between target_size and dim in DataInfo should agree.');
end

% Update the coordinates and calulate the normalized voxel coordinates.
delta1 = (max_ext(1)-min_ext(1))/(dim(1)-1);
delta2 = (max_ext(2)-min_ext(2))/(dim(2)-1);
delta3 = (max_ext(3)-min_ext(3))/(dim(3)-1);

[min_update, max_update] = ResizeDataCoordinate(dim, min_ext, max_ext, dim_target);
line_d1 = linspace(min_update(1), max_update(1), dim_target(1));
line_d2 = linspace(min_update(2), max_update(2), dim_target(2));
line_d3 = linspace(min_update(3), max_update(3), dim_target(3));
mesh_d1 = repmat(line_d1(:), [1 dim_target(2)]);
mesh_d2 = repmat(line_d2(:)', [dim_target(1) 1]);
mat_d1 = (mesh_d1-min_ext(1))/delta1+1;
mat_d2 = (mesh_d2-min_ext(2))/delta2+1;

% Get the values for each slice
volRZ = zeros(dim_target);
for slc=1:dim_target(3)
    mat_d3 = ((line_d3(slc)-min_ext(3))/delta3+1) * ones(dim_target(1), dim_target(2));
    [slcRZ] = trilinear(vol, mat_d2, mat_d1, mat_d3);
    volRZ(:,:,slc) = slcRZ;
end

% Update the DataInfo
DataInfoRZ = DataInfo;
DataInfoRZ.dim = dim_target;
DataInfoRZ.min_ext = min_update;
DataInfoRZ.max_ext = max_update;

end