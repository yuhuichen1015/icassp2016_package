run('../Initial.m');
TEST_BEGIN;
%%
xt1 = [0; 0; 0; 0; 0; 0];
xt2 = [15; 20; -10; 0; 0; 0];
xt3 = [-20; -7;  28; 0; 0; 0];
xt = [xt1(:) xt2(:) xt3(:)];
Joint_origin = [100 3 89]

PInfo = struct();
PInfo.joint_origin = Joint_origin;
PInfo.sysoffset_xyz = [0,0,0];
PInfo.sysoffset_angles = [0,0,0];
PInfo.sysoffset_dcm = eye(3);
[xt_global] = CalcGlobalPars(xt, PInfo);
[origin_est]=EstimateOrigin(xt_global, PInfo, 1)

% Check system offset
PInfo = struct();
PInfo.joint_origin = Joint_origin;
PInfo.sysoffset_xyz = [5,10,-10];
PInfo.sysoffset_angles = [-5,3,30];
PInfo.sysoffset_dcm = EA2DCM(PInfo.sysoffset_angles);
[xt_global] = CalcGlobalPars(xt, PInfo);
[origin_est]=EstimateOrigin(xt_global, PInfo, 1)

% Check estimate xt instead of global xt
PInfo = struct();
% Calculate with origin 
PInfo.joint_origin = Joint_origin;
xt_worigin = CalcGlobalPars(xt, PInfo);
% Calculate system offset
PInfo.joint_origin = zeros(3,1);
PInfo.sysoffset_xyz = [5,10,-10];
PInfo.sysoffset_angles = [-5,3,30];
PInfo.sysoffset_dcm = EA2DCM(PInfo.sysoffset_angles);
[origin_est]=EstimateOrigin(xt_worigin, PInfo, 0)




% %%%
% run('../Initial.m');
% % Load the ground truth parameters
% load([data_path 'SimulatedData/GroundTruth_old.mat']);
% 
% % xt = xt_truth;
% % xt(4:6,:) = 0;
% % [xt_m] = ChangeParsOrigin(xt, [97.5 97.5 51]);
% % k=1:200;
% % CheckEstimationResult(xt_truth, k);
% % CheckEstimationResult(xt_m, k);
% 
% k=1:1820
% CheckEstimationResult(xt_truth,k);
% 
% [origin_est]=EstimateOrigin(xt_truth(:,k));
% origin_est
% 
% [xt_fixed] = ChangeParsOrigin(xt_truth(:,k), origin_est);
% CheckEstimationResult(xt_fixed);

%%
TEST_END;