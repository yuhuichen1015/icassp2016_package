function [origin_est]=EstimateOrigin(xt, PInfo, isglobal)

if(size(xt,1)~=6)
    error('input should have 6 rows.');
end

if(~isfield(PInfo, 'sysoffset_xyz'))
    sysoffset_xyz = zeros(3,1);
    sysoffset_dcm = eye(3);
else
    sysoffset_xyz = PInfo.sysoffset_xyz;
    sysoffset_dcm = PInfo.sysoffset_dcm;
end

bvec = [];
Amtx = [];
for k=1:size(xt,2)
    if(isglobal)
        A = SpinCalc('EA321toDCM', xt(1:3,k)', 0.001, 0);
        Ah = A/sysoffset_dcm;
        bvec = [bvec; Ah*sysoffset_xyz(:)-xt(4:6,k);];
        Amtx = [Amtx; (Ah-eye(3))];
    else
        Ah = SpinCalc('EA321toDCM', xt(1:3,k)', 0.001, 0);
        bvec = [bvec; -xt(4:6,k);];
        Amtx = [Amtx; (Ah-eye(3))];
    end
end
origin_est = pinv(Amtx)*bvec;

end