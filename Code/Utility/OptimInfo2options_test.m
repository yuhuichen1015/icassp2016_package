run('../Initial.m');
TEST_BEGIN;
%%

OptimInfo = struct();
OptimInfo.xinit = zeros(6,1);
OptimInfo.rand_range = 5;
OptimInfo.space_dim = 6;
[options] = OptimInfo2options(OptimInfo); 
size(options.n_init)
var(options.n_init, [] ,2)
OptimInfo.space_dim = 9;
OptimInfo.min_ext_slices = [0 0 0];
OptimInfo.max_ext_slices = [200 200 0];
[options] = OptimInfo2options(OptimInfo); 
size(options.n_init)
var(options.n_init, [], 2)

OptimInfo.rand_sigma = 25*eye(6);
[options] = OptimInfo2options(OptimInfo); 
size(options.n_init)
var(options.n_init)

OptimInfo.max_iteration = 10;
[options] = OptimInfo2options(OptimInfo); 
options.MaxIter

OptimInfo.tolerance_func = 1;
[options] = OptimInfo2options(OptimInfo); 
options.TolFun

%% CtrlPts
OptimInfo = struct();
OptimInfo.xinit = Pars2CtrlPts(zeros(6,1), [0 0 0], [200 200 0]);
OptimInfo.rand_range = 5;
OptimInfo.space_dim = 6;
[options] = OptimInfo2options(OptimInfo); 
size(options.n_init)
var(options.n_init, [] ,2)
OptimInfo.space_dim = 9;
OptimInfo.min_ext_slices = [0 0 0];
OptimInfo.max_ext_slices = [200 200 0];
[options] = OptimInfo2options(OptimInfo); 
size(options.n_init)
var(options.n_init, [], 2)


%%
TEST_END;