/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * EA2DCM.c
 *
 * Code generation for function 'EA2DCM'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "EA2DCM.h"

/* Function Definitions */
void EA2DCM(const real_T EA[3], real_T DCM[9])
{
  real_T cs[3];
  real_T sn[3];
  int32_T i;
  for (i = 0; i < 3; i++) {
    cs[i] = muDoubleScalarCos(EA[i] * 3.1415926535897931 / 180.0);
    sn[i] = muDoubleScalarSin(EA[i] * 3.1415926535897931 / 180.0);
  }

  DCM[0] = cs[0] * cs[1];
  DCM[3] = sn[0] * cs[1];
  DCM[6] = -sn[1];
  DCM[1] = cs[0] * sn[1] * sn[2] - sn[0] * cs[2];
  DCM[4] = sn[0] * sn[1] * sn[2] + cs[0] * cs[2];
  DCM[7] = sn[2] * cs[1];
  DCM[2] = cs[0] * sn[1] * cs[2] + sn[0] * sn[2];
  DCM[5] = sn[0] * sn[1] * cs[2] - cs[0] * sn[2];
  DCM[8] = cs[2] * cs[1];
}

/* End of code generation (EA2DCM.c) */
