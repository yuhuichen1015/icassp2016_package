/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * EA2DCM_data.c
 *
 * Code generation for function 'EA2DCM_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "EA2DCM.h"
#include "EA2DCM_data.h"

/* Variable Definitions */
emlrtCTX emlrtRootTLSGlobal = NULL;
const volatile char_T *emlrtBreakCheckR2012bFlagVar;
emlrtContext emlrtContextGlobal = { true, false, 131418U, NULL, "EA2DCM", NULL,
  false, { 2045744189U, 2170104910U, 2743257031U, 4284093946U }, NULL };

/* End of code generation (EA2DCM_data.c) */
