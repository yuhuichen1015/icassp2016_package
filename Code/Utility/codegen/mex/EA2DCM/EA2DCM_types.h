/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * EA2DCM_types.h
 *
 * Code generation for function 'EA2DCM'
 *
 */

#ifndef __EA2DCM_TYPES_H__
#define __EA2DCM_TYPES_H__

/* Include files */
#include "rtwtypes.h"
#endif

/* End of code generation (EA2DCM_types.h) */
