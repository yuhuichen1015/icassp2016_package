/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * EA2DCM_initialize.h
 *
 * Code generation for function 'EA2DCM_initialize'
 *
 */

#ifndef __EA2DCM_INITIALIZE_H__
#define __EA2DCM_INITIALIZE_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "EA2DCM_types.h"

/* Function Declarations */
extern void EA2DCM_initialize(void);

#endif

/* End of code generation (EA2DCM_initialize.h) */
