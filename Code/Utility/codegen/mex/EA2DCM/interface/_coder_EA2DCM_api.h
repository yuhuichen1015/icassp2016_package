/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * _coder_EA2DCM_api.h
 *
 * Code generation for function '_coder_EA2DCM_api'
 *
 */

#ifndef ___CODER_EA2DCM_API_H__
#define ___CODER_EA2DCM_API_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "mwmathutil.h"
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "EA2DCM_types.h"

/* Function Declarations */
extern void EA2DCM_api(const mxArray * const prhs[1], const mxArray *plhs[1]);

#endif

/* End of code generation (_coder_EA2DCM_api.h) */
