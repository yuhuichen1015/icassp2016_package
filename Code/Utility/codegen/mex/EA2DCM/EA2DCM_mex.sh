MATLAB="/usr/local/MATLAB/R2015a"
Arch=glnxa64
ENTRYPOINT=mexFunction
MAPFILE=$ENTRYPOINT'.map'
PREFDIR="/home/yuhuic/.matlab/R2015a"
OPTSFILE_NAME="./setEnv.sh"
. $OPTSFILE_NAME
COMPILER=$CC
. $OPTSFILE_NAME
echo "# Make settings for EA2DCM" > EA2DCM_mex.mki
echo "CC=$CC" >> EA2DCM_mex.mki
echo "CFLAGS=$CFLAGS" >> EA2DCM_mex.mki
echo "CLIBS=$CLIBS" >> EA2DCM_mex.mki
echo "COPTIMFLAGS=$COPTIMFLAGS" >> EA2DCM_mex.mki
echo "CDEBUGFLAGS=$CDEBUGFLAGS" >> EA2DCM_mex.mki
echo "CXX=$CXX" >> EA2DCM_mex.mki
echo "CXXFLAGS=$CXXFLAGS" >> EA2DCM_mex.mki
echo "CXXLIBS=$CXXLIBS" >> EA2DCM_mex.mki
echo "CXXOPTIMFLAGS=$CXXOPTIMFLAGS" >> EA2DCM_mex.mki
echo "CXXDEBUGFLAGS=$CXXDEBUGFLAGS" >> EA2DCM_mex.mki
echo "LD=$LD" >> EA2DCM_mex.mki
echo "LDFLAGS=$LDFLAGS" >> EA2DCM_mex.mki
echo "LDOPTIMFLAGS=$LDOPTIMFLAGS" >> EA2DCM_mex.mki
echo "LDDEBUGFLAGS=$LDDEBUGFLAGS" >> EA2DCM_mex.mki
echo "Arch=$Arch" >> EA2DCM_mex.mki
echo OMPFLAGS= >> EA2DCM_mex.mki
echo OMPLINKFLAGS= >> EA2DCM_mex.mki
echo "EMC_COMPILER=gcc" >> EA2DCM_mex.mki
echo "EMC_CONFIG=optim" >> EA2DCM_mex.mki
"/usr/local/MATLAB/R2015a/bin/glnxa64/gmake" -B -f EA2DCM_mex.mk
