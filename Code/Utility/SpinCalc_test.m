run('../Initial.m');
TEST_BEGIN;
%%

angles = [5 -15 -10];
tic;
for i=1:1000
    cs=cos(angles*pi/180);
    sn=sin(angles*pi/180);
    B=[1 0 0;0 cs(3) sn(3);0 -sn(3) cs(3)];
    C=[cs(2) 0 -sn(2);0 1 0;sn(2) 0 cs(2)];
    D=[cs(1) sn(1) 0;-sn(1) cs(1) 0;0 0 1];
    A=B*C*D;
end
toc
A

tic;
for i=1:1000
    cs=cos(angles*pi/180);
    sn=sin(angles*pi/180);
    Ab = [  cs(1)*cs(2)                     sn(1)*cs(2)                     -sn(2);
            cs(1)*sn(2)*sn(3)-sn(1)*cs(3)   sn(1)*sn(2)*sn(3)+cs(1)*cs(3)   sn(3)*cs(2);
            cs(1)*sn(2)*cs(3)+sn(1)*sn(3)   sn(1)*sn(2)*cs(3)-cs(1)*sn(3)   cs(3)*cs(2);];
end
toc
Ab



tic;
for i=1:1000
    [Ac] = SpinCalc('EA321toDCM', angles, 0.001, 0);
end
toc
Ac
%%
TEST_END;