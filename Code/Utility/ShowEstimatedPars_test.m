run('../Initial.m');
TEST_BEGIN;

%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth.mat']);

% Test with one argument
ShowEstimatedPars(xt_truth);
ShowEstimatedPars(xt_truth(:,1:200));

% Test with two arguments
ShowEstimatedPars(xt_truth_origin, xt_truth);
ShowEstimatedPars(xt_truth_origin(:,1:200), xt_truth(:,1:200));

% Test with four arguments
Vol2SlcInfo = struct();
Vol2SlcInfo.joint_origin = Target_joint_origin;
ShowEstimatedPars(xt_truth_origin(:,1:200), xt_truth(:,1:200), Vol2SlcInfo);

%%
TEST_END;