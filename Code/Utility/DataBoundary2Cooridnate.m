function [GridCoordinate, min_ext, max_ext] = DataBoundary2Cooridnate(dim, min_boundary, max_boundary)
    if(numel(dim)~=numel(min_boundary) || numel(dim)~=numel(max_boundary))
        error('The input dimension should be consistent.');
    end
    
    GridCoordinate = cell(numel(dim),1);
    min_ext = zeros(3,1);
    max_ext = zeros(3,1);
    for d=1:numel(dim)
        width = (max_boundary(d)-min_boundary(d)) / dim(d);
        min_ext(d) = min_boundary(d)+width/2;
        max_ext(d) = max_boundary(d)-width/2;
        GridCoordinate{d} = linspace(min_ext(d),max_ext(d),dim(d));
    end
end