function [RMSE] = CheckEstimationResult(xt_map, varargin)
    nVarargin = length(varargin);
    if(nVarargin==0)
        x=1:size(xt_map,2);
        figure;
        plot(x, xt_map(1,:), 'r--', ...
             x, xt_map(2,:), 'g--', ...
             x, xt_map(3,:), 'b--'),...
            title('Estimated rotation');
        legend('\alpha', '\beta', '\gamma');
        figure; 
        plot(x, xt_map(4,:), 'r--',...
             x, xt_map(5,:), 'g--',...
             x, xt_map(6,:), 'b--'),...
             title('Estimated translation');
        legend('\delta x', '\delta y', '\delta z');
    elseif(nVarargin==1)
        x = varargin{1};
        figure;
        plot(x, xt_map(1,x), 'r--', ...
             x, xt_map(2,x), 'g--', ...
             x, xt_map(3,x), 'b--'),...
            title('Estimated rotation');
        legend('\alpha', '\beta', '\gamma');
        figure; 
        plot(x, xt_map(4,x), 'r--',...
             x, xt_map(5,x), 'g--',...
             x, xt_map(6,x), 'b--'),...
             title('Estimated translation');
        legend('\delta x', '\delta y', '\delta z');
    elseif(nVarargin==2)
        x = varargin{1};
        xt_truth = varargin{2};
        figure;
        plot(x, xt_map(1,x), 'r--', ...
             x, xt_map(2,x), 'g--', ...
             x, xt_map(3,x), 'b--'),...
            title('Estimated rotation');
        hold on;
        plot(x, xt_truth(1,x), 'r-', ...
             x, xt_truth(2,x), 'g-', ...
             x, xt_truth(3,x), 'b-'),...
        
        figure; 
        plot(x, xt_map(4,x), 'r--',...
             x, xt_map(5,x), 'g--',...
             x, xt_map(6,x), 'b--'),...
             title('Estimated translation');
        hold on;
        plot(x, xt_truth(4,x), 'r-',...
             x, xt_truth(5,x), 'g-',...
             x, xt_truth(6,x), 'b-');
         
        % Calculate the RMSE
        alpha = sqrt(norm(xt_map(1,x)-xt_truth(1,x))^2/size(x,2));
        beta = sqrt(norm(xt_map(2,x)-xt_truth(2,x))^2/size(x,2));
        gamma = sqrt(norm(xt_map(3,x)-xt_truth(3,x))^2/size(x,2));
        xest = sqrt(norm(xt_map(4,x)-xt_truth(4,x))^2/size(x,2));
        yest = sqrt(norm(xt_map(5,x)-xt_truth(5,x))^2/size(x,2));
        zest = sqrt(norm(xt_map(6,x)-xt_truth(6,x))^2/size(x,2));
        total_rot = sqrt((alpha^2+beta^2+gamma^2)/3);
        total_trans = sqrt((xest^2+yest^2+zest^2)/3);
        total = sqrt((alpha^2+beta^2+gamma^2+xest^2+yest^2+zest^2)/6);
        RMSE = [alpha, beta, gamma, xest, yest, zest, total_rot, total_trans, total];
    elseif(nVarargin==3)
        x = varargin{1};
        xt_truth = varargin{2};
        % Calculate the RMSE
        alpha = sqrt(norm(xt_map(1,x)-xt_truth(1,x))^2/size(x,2));
        beta = sqrt(norm(xt_map(2,x)-xt_truth(2,x))^2/size(x,2));
        gamma = sqrt(norm(xt_map(3,x)-xt_truth(3,x))^2/size(x,2));
        xest = sqrt(norm(xt_map(4,x)-xt_truth(4,x))^2/size(x,2));
        yest = sqrt(norm(xt_map(5,x)-xt_truth(5,x))^2/size(x,2));
        zest = sqrt(norm(xt_map(6,x)-xt_truth(6,x))^2/size(x,2));
        total_rot = sqrt((alpha^2+beta^2+gamma^2)/3);
        total_trans = sqrt((xest^2+yest^2+zest^2)/3);
        total = sqrt((alpha^2+beta^2+gamma^2+xest^2+yest^2+zest^2)/6);
        RMSE = [alpha, beta, gamma, xest, yest, zest, total_rot, total_trans, total];
    end
end