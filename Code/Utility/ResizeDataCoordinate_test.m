run('../Initial.m');
TEST_BEGIN;
%%
clear;
dim = [256 256 124];
min_ext = [0.000000 0.000000 -36.640202];
max_ext = [199.218750 199.218750 147.860001];
l=linspace(min_ext(1),max_ext(1), dim(1));
do1=l(2)-l(1)
l=linspace(min_ext(2),max_ext(2), dim(2));
do2=l(2)-l(1)
l=linspace(min_ext(3),max_ext(3), dim(3));
do3=l(2)-l(1)


dim_target = [256 256 124];
[min_update, max_update] = ResizeDataCoordinate(dim, min_ext, max_ext, dim_target);
min_update
max_update

dim_target = [128 128 62];
[min_update, max_update] = ResizeDataCoordinate(dim, min_ext, max_ext, dim_target);
min_update
max_update
l=linspace(min_update(1),max_update(1), dim_target(1));
d=l(2)-l(1)
do1*2
l=linspace(min_update(3),max_update(3), dim_target(3));
d=l(2)-l(1)
do3*2

%%
TEST_END;