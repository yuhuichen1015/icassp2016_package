function [RMSE]=ShowParsRMSE(xt, xt_truth, varargin)

if(size(xt,1)~=6 || size(xt,1)~=size(xt_truth,1) || size(xt,2)~=size(xt_truth,2))
    error('The first input should be 6xN matrix. And the first two inputs should have the same size.');
end

switch(numel(varargin))
    case 0
        xt_est = xt;
    case 1
        xt_est = CalcGlobalPars(xt, varargin{1});
    otherwise
        error('This function only support at most three arguments.');
end

N = size(xt_est,2);
RMSE = struct();
RMSE.alpha = sqrt(norm(xt_est(1,:)-xt_truth(1,:))^2/N);
RMSE.beta = sqrt(norm(xt_est(2,:)-xt_truth(2,:))^2/N);
RMSE.gamma = sqrt(norm(xt_est(3,:)-xt_truth(3,:))^2/N);
RMSE.deltax = sqrt(norm(xt_est(4,:)-xt_truth(4,:))^2/N);
RMSE.deltay = sqrt(norm(xt_est(5,:)-xt_truth(5,:))^2/N);
RMSE.deltaz = sqrt(norm(xt_est(6,:)-xt_truth(6,:))^2/N);
RMSE.rotation = sqrt((RMSE.alpha^2+RMSE.beta^2+RMSE.gamma^2)/3);
RMSE.translation = sqrt((RMSE.deltax^2+RMSE.deltay^2+RMSE.deltaz^2)/3);
RMSE.total = sqrt((RMSE.alpha^2+RMSE.beta^2+RMSE.gamma^2+RMSE.deltax^2+RMSE.deltay^2+RMSE.deltaz^2)/6);

end