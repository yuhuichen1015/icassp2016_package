function [Pars]=CtrlPts2Pars(PtsMtx)
% Input should be a matrix with the following form (3x3):
%   PtsMtx = [Point1(:) Point2(:) Point3(:)];

if(size(PtsMtx,1)~=3 || size(PtsMtx,2)~=3)
    error('Pts should be a 3x3 matrix');
else
    Pts = PtsMtx(1:3,1:3);
end

% Estimate the translation
trans = mean(Pts,2);

a=Pts(:,3)-Pts(:,2);
b=Pts(:,1)-Pts(:,2);
Nr = cross(a,b);
Nr = Nr/norm(Nr);
if(Nr(3)<0)
    Nr = -Nr;
end

% Estimate the third angle
angle = zeros(1,3);
x=Nr(1);    y=Nr(2);    z=Nr(3);
angle(3)=atan(y/z);

cs=cos(-angle);
sn=sin(-angle);
Nrr = [1 0 0;0 cs(3) sn(3);0 -sn(3) cs(3)]*Nr(:);
if(abs(Nrr(2))>0.0001)
    error('Something wrong');
end
% Estimate the second angle
x=Nrr(1);    y=Nrr(2);    z=Nrr(3);
angle(2)=-atan(x/z);

% Estimate the first angle
PPts = [Pts mean(Pts,2)];
cs=cos(-angle);
sn=sin(-angle);
TPts = ([cs(2) 0 -sn(2);0 1 0;sn(2) 0 cs(2)]*[1 0 0;0 cs(3) sn(3);0 -sn(3) cs(3)]*PPts);
if(abs(TPts(3,1)-TPts(3,end))>0.01)
    Pts
    error('Something wrong');
end
dx = TPts(1,1) - TPts(1,end);
dy = TPts(2,1) - TPts(2,end);
if(dy<0)
    Pts
    dy
    error('Something wrong');
else
    angle(1) = atan(dx/dy);
end

angle = angle*180/pi;
Pars = [angle(:); trans(:)];

end