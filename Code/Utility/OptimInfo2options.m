function [options]=OptimInfo2options(OptimInfo)

options = optimset;
% Set up the maximum iteration
if(~isfield(OptimInfo, 'max_iteration'))
    options.MaxIter = 1000;
    warning('Using default maximum iteration number: 1000');
else
    options.MaxIter = OptimInfo.max_iteration;
end

% Set up the tolerance objective value
if(~isfield(OptimInfo, 'tolerance_func'))
    options.TolFun = 1e-4;
    warning('Using default TolFun as 10^(-4)');
else
    options.TolFun = OptimInfo.tolerance_func;
end

if(isfield(OptimInfo, 'n_init'))
    options.n_init = OptimInfo.n_init;
    return;
end

% Set up the initial simplex
if(~isfield(OptimInfo, 'xinit'))
    warning('Must provide the initial point for optimizer.');
else
    xinit = OptimInfo.xinit;
    d = size(xinit(:),1);
    if(isfield(OptimInfo, 'rand_sigma'))
        xinit_Simplex = mvnrnd(xinit(:), OptimInfo.rand_sigma, OptimInfo.space_dim+1)';
    else
        if(isfield(OptimInfo, 'rand_range'))
            xinit_Simplex = mvnrnd(xinit(:), OptimInfo.rand_range^2*eye(d), OptimInfo.space_dim+1)';
        else
            error('Must provide randomization parameters: rand_sigma or rand_range.');
        end
    end
    
    if(OptimInfo.space_dim==9 && d==6)
        xinit_tmp = zeros(9,10);
        for t=1:10
            pts = Pars2CtrlPts(xinit_Simplex(:,t), OptimInfo.min_ext_slices, OptimInfo.max_ext_slices);
            xinit_tmp(:,t) = pts(:);
        end
        options.n_init = xinit_tmp;
    elseif(OptimInfo.space_dim==6 && d==9)
        xinit_tmp = zeros(6,7);
        for t=1:7
            xinit_tmp(:,t) = CtrlPts2Pars(reshape(xinit_Simplex(:,t),[3,3]));
        end
        options.n_init = xinit_tmp;
    else
        options.n_init = xinit_Simplex;
    end
end


 
end