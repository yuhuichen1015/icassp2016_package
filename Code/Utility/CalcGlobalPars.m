function [xt_global] = CalcGlobalPars(xt, PInfo, varargin)

if(size(xt,1)~=6)
    error('Input parameters should be 6xN matrix.');
end

if(~isfield(PInfo, 'joint_origin'))
    joint_origin = zeros(3,1);
else
    joint_origin = PInfo.joint_origin;
end

if(~isfield(PInfo, 'sysoffset_angles'))
    sysoffset_xyz = zeros(3,1);
    sysoffset_dcm = eye(3);
else
    sysoffset_xyz = PInfo.sysoffset_xyz;
    sysoffset_dcm = PInfo.sysoffset_dcm;
end

if(numel(varargin)~=1)
    inverse = 0;
else
    inverse = varargin{1};
end

xt_global = zeros(size(xt));
for k = 1:size(xt,2)
    angles = xt(1:3,k);
    if(~inverse)
        Ah = SpinCalc('EA321toDCM', angles(:)', 0.001, 0);
        ang_global = SpinCalc('DCMtoEA321', Ah*sysoffset_dcm, 0.001, 0);
        if(ang_global(1)>180)
            ang_global(1) = ang_global(1)-360;
        end
        if(ang_global(2)>180)
            ang_global(2) = ang_global(2)-360;
        end
        if(ang_global(3)>180)
            ang_global(3) = ang_global(3)-360;
        end
        xyz_global = Ah*sysoffset_xyz(:)-Ah*joint_origin(:) + xt(4:6,k) + joint_origin(:);
        xt_global(:,k) = [ang_global(:); xyz_global(:);];
    else
        A = SpinCalc('EA321toDCM', angles(:)', 0.001, 0);
        Ah = A/sysoffset_dcm;
        ang_global = SpinCalc('DCMtoEA321', Ah, 0.001, 0);
        if(ang_global(1)>180)
            ang_global(1) = ang_global(1)-360;
        end
        if(ang_global(2)>180)
            ang_global(2) = ang_global(2)-360;
        end
        if(ang_global(3)>180)
            ang_global(3) = ang_global(3)-360;
        end
        xyz_global = xt(4:6,k) - joint_origin(:) - Ah*(sysoffset_xyz(:) - joint_origin(:));
        xt_global(:,k) = [ang_global(:); xyz_global(:);];
    end
end

end