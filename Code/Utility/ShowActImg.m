function [ActImgSep, ActImgFDR, ActImgSgl] = ShowActImg(slcIdx, Alpha, PInfo)
    
run([PInfo.matlab_path 'Initial.m']);
% Load the truth activation map
load([PInfo.reconstruct_info.reconstructed_folder 'AnatVolRZ.mat']);

if(slcIdx > size(AnatVolRZ,3))
    error('Slice idx out of range.');
end

RandPermSet = PInfo.reconstruct_info.randperm_set;
for s=1:numel(RandPermSet)
    RandPermPars = RandPermSet{s};
    if(RandPermPars.num_of_trials==1)
        break;
    end
end

% Load the Tstats file
[HighSelected, LowSelected]=SelectSequence(PInfo.reconstruct_info.high_seq,...
        PInfo.reconstruct_info.low_seq, RandPermPars, [data_path 'Sequences/']);
load([PInfo.reconstruct_info.reconstructed_folder...
    'Tstats_' DataHash([HighSelected(:); LowSelected(:); RandPermPars.num_of_randperm]) '.mat']);

% Hypothesis test with P-value threshold Alpha.
[ActResultSgl, ActResultSep, ActResultFDR]=HypoTest(TrueT, PValue, PValueMax, Alpha, RandPermPars.num_of_randperm);

% Overlay the images
depth = 0.7;
ActImgSep = zeros([size(AnatVolRZ,2), size(AnatVolRZ,1) 3]); 
slcIdxImg = AnatVolRZ(:,:,slcIdx)';
slcIdxImg = slcIdxImg / max(slcIdxImg(:));
% Seperate detection
slcIdxActSep = ActResultSep(:,:,slcIdx)';
slcIdxActSepP = slcIdxActSep==1;
slcIdxActSepN = slcIdxActSep==-1;
ActImgSep(:,:,1) = slcIdxImg + (slcIdxActSepP.*slcIdxImg).*depth - (slcIdxActSepN.*slcIdxImg).*depth;
ActImgSep(:,:,2) = slcIdxImg - (slcIdxActSepP.*slcIdxImg).*depth - (slcIdxActSepN.*slcIdxImg).*depth;
ActImgSep(:,:,3) = slcIdxImg - (slcIdxActSepP.*slcIdxImg).*depth + (slcIdxActSepN.*slcIdxImg).*depth;
% figure; image(ActImgSep);

% FDR
slcIdxActFDR = ActResultFDR(:,:,slcIdx)';
slcIdxActFDRP = slcIdxActFDR==1;
slcIdxActFDRN = slcIdxActFDR==-1;
ActImgFDR(:,:,1) = slcIdxImg + (slcIdxActFDRP.*slcIdxImg).*depth - (slcIdxActFDRN.*slcIdxImg).*depth;
ActImgFDR(:,:,2) = slcIdxImg - (slcIdxActFDRP.*slcIdxImg).*depth - (slcIdxActFDRN.*slcIdxImg).*depth;
ActImgFDR(:,:,3) = slcIdxImg - (slcIdxActFDRP.*slcIdxImg).*depth + (slcIdxActFDRN.*slcIdxImg).*depth;
% figure; image(ActImgFDR);

% Single thresholding
slcIdxActSgl = ActResultSgl(:,:,slcIdx)';
slcIdxActSglP = slcIdxActSgl==1;
slcIdxActSglN = slcIdxActSgl==-1;
ActImgSgl(:,:,1) = slcIdxImg + (slcIdxActSglP.*slcIdxImg).*depth - (slcIdxActSglN.*slcIdxImg).*depth;
ActImgSgl(:,:,2) = slcIdxImg - (slcIdxActSglP.*slcIdxImg).*depth - (slcIdxActSglN.*slcIdxImg).*depth;
ActImgSgl(:,:,3) = slcIdxImg - (slcIdxActSglP.*slcIdxImg).*depth + (slcIdxActSglN.*slcIdxImg).*depth;
% figure; image(ActImgSgl);

end