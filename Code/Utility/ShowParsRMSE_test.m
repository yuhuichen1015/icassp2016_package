run('../Initial.m');
TEST_BEGIN;
%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth.mat']);

% Test with one argument
ShowParsRMSE(xt_truth(:,1:200), xt_truth(:,2:201))

% Test with two arguments
Vol2SlcInfo = struct();
Vol2SlcInfo.joint_origin = Target_joint_origin;
ShowParsRMSE(xt_truth_origin(:,1:200), xt_truth(:,2:201), Vol2SlcInfo)


%%
TEST_END;