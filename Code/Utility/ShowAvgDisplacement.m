function [AvgDp]=ShowAvgDisplacement(time_idx, xt, xt_truth, DataInfo)
% xt_truth must be in global view.

% check field existance
if(~isfield(DataInfo, 'dim') || ~isfield(DataInfo, 'min_ext') ||...
        ~isfield(DataInfo, 'max_ext'))
    error('DataInfo must contains refernce vol coordinate info.');
else
    dim = DataInfo.dim;
    min_ext = DataInfo.min_ext;
    max_ext = DataInfo.max_ext;
end
if(~isfield(DataInfo, 'dim_slices') || ~isfield(DataInfo, 'min_ext_slices') ||...
        ~isfield(DataInfo, 'max_ext_slices'))
    error('DataInfo must contains slice coordinate info');
else
    dim_slices = DataInfo.dim_slices;
    min_ext_slices = DataInfo.min_ext_slices;
    max_ext_slices = DataInfo.max_ext_slices;
end

if(~isfield(DataInfo, 'joint_origin'))
    joint_origin = zeros(3,1);
else
    joint_origin = DataInfo.joint_origin;
end

% Check the system offset information
if(~isfield(DataInfo, 'sysoffset_angles'))
    sysoffset_xyz = zeros(3,1);
    Asys = eye(3);
else
    sysoffset_xyz = DataInfo.sysoffset_xyz;
    Asys = DataInfo.sysoffset_dcm;
end

% Check whether the precalculated information exists
if(~isfield(DataInfo, 'delta1'))
    % Prepare the transformed slice coordinate
    DataInfo = CalcCoordinateInfo(DataInfo);
end
line_1 = DataInfo.line_1;
line_2 = DataInfo.line_2;
slice_Z_values = DataInfo.slice_z_values;

% Compute the coordinate for each grid.
Num_of_input = numel(time_idx);
xt_truth_origin = CalcGlobalPars(xt_truth, DataInfo, 1);

AvgDp = zeros(size(time_idx));
for t = 1:Num_of_input
    % Calculate the slice index
    tidx = time_idx(t);
    slc_idx = DataInfo.slice_sequence(mod(tidx-1, DataInfo.jmax)+1);
    
    z_cor = slice_Z_values(slc_idx)*ones([dim_slices(1)*dim_slices(2),1]);
    xyz_cor = [line_1(:)'; line_2(:)'; z_cor(:)'];
    GridCoordinates = Asys*xyz_cor + repmat(sysoffset_xyz(:), [1, size(xyz_cor,2)]) - ...
        repmat(joint_origin(:), [1, size(xyz_cor,2)]);
    
    % Prepare the rotation matrix
    A_est = EA2DCM(xt(1:3,tidx));
    xyz_est = xt(4:6,tidx);
    
    A_truth = EA2DCM(xt_truth_origin(1:3,tidx));
    xyz_truth = xt_truth_origin(4:6,tidx);
    
    % Calculate the voxel displacement
    rotated_est = A_est*GridCoordinates +...
        repmat(xyz_est(:),[1 size(GridCoordinates,2)]);
    
    rotated_truth = A_truth*GridCoordinates +...
        repmat(xyz_truth(:),[1 size(GridCoordinates,2)]);
    
    displacement = mean(sqrt(sum((rotated_est-rotated_truth).^2,1)));
    AvgDp(t) = displacement;
end

end