function LineSpecs = GetLineSpecs(Labels)

GroundTruthCnt = 0;
NoCorrectionCnt = 0;
V2VCnt = 0;
MSVCnt = 0;
MTCnt = 0;

Specs = {'-', '--', '-.', '.', '-x', '-o', '-*'};
LineSpecs = {};

for l = 1:numel(Labels)
    label = Labels{l};
    token = strtok(label, '_');
    switch(token)
        case 'GroundTruthPars'
            GroundTruthCnt = GroundTruthCnt + 1;
            LineSpecs = [LineSpecs, ['r' Specs{GroundTruthCnt}]];
        case 'NoCorrection'
            NoCorrectionCnt = NoCorrectionCnt + 1;
            LineSpecs = [LineSpecs, ['k' Specs{NoCorrectionCnt}]];
        case 'V2V'
            V2VCnt = V2VCnt + 1;
            LineSpecs = [LineSpecs, ['g' Specs{V2VCnt}]];
        case 'MSV'
            MSVCnt = MSVCnt + 1;
            LineSpecs = [LineSpecs, ['c' Specs{MSVCnt}]];
        case 'MT'
            MTCnt = MTCnt + 1;
            LineSpecs = [LineSpecs, ['b' Specs{MTCnt}]];
        otherwise
            error(['Unrecognized token:' token]);
    end
end

end