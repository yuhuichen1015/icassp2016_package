function [y] = barrierfunc(Pts, InitPts, bound)
    y=0;
    PtsMtx = reshape(Pts, [3 3]);
    InitPtsMtx = reshape(InitPts, [3 3]);
    dist1 = norm(PtsMtx(:,1)-InitPtsMtx(:,1));
    dist2 = norm(PtsMtx(:,2)-InitPtsMtx(:,2));
    dist3 = norm(PtsMtx(:,3)-InitPtsMtx(:,3));
    
    if(dist1 > bound*0.9)
        y = y + (dist1 - bound*0.9)*1000;
    end
    if(dist2 > bound*0.9)
        y = y + (dist2 - bound*0.9)*1000;
    end
    if(dist3 > bound*0.9)
        y = y + (dist3 - bound*0.9)*1000;
    end
end