function []=ShowEstimatedPars(xt, varargin)
% first arg: xt is the estimated parameters to be showed.
% second arg: xt_truth which is used to compare with xt.
% third arg: PInfo which contains the system offset and joint_origin info.
if(size(xt,1)~=6)
    error('The first input should be 6xN matrix.');
end

switch(numel(varargin))
    case 0
        xt_show = xt;
    case 1
        xt_show = xt;
        xt_truth = varargin{1};
    case 2
        [xt_show] = CalcGlobalPars(xt, varargin{2});
        xt_truth = varargin{1};
    otherwise
        error('This function only support at most three arguments.');
end

% Draw the rotation angles
x=1:size(xt_show,2);
figure;
plot(x, xt_show(1,x), 'r-', ...
    x, xt_show(2,x), 'g-', ...
    x, xt_show(3,x), 'b-'),...
    title('Estimated rotation');
if(exist('xt_truth', 'var'))
    hold on;
    plot(x, xt_truth(1,x), 'r--', ...
    x, xt_truth(2,x), 'g--', ...
    x, xt_truth(3,x), 'b--'),...
end

% Draw the translation parameters
figure;
plot(x, xt_show(4,x), 'r-', ...
    x, xt_show(5,x), 'g-', ...
    x, xt_show(6,x), 'b-'),...
    title('Estimated traslation');
if(exist('xt_truth', 'var'))
    hold on;
    plot(x, xt_truth(4,x), 'r--', ...
    x, xt_truth(5,x), 'g--', ...
    x, xt_truth(6,x), 'b--'),...
end


end