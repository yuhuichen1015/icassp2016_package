run('../Initial.m');
TEST_BEGIN;
%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth_old.mat']);
% Load and add datainfo
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
% Add Data description:
DataInfo.activation_level = 10;
% Load the activation volumns.
load([data_path 'SimulatedData/PeriodVolsFull_' int2str(DataInfo.activation_level) '.mat']);

dim_target = [80 80 31];
[volRZ, DataInfoRZ] = volresize(RestVol, dim_target, DataInfo);

DataInfo
DataInfoRZ

slc = 10;
figure; imagesc(volRZ(:,:,slc));
figure; imagesc(RestVol(:,:,slc*4));

dim_target = [128 128 124];
[volRZ, DataInfoRZ] = volresize(RestVol, dim_target, DataInfo);

slc = 80;
figure; imagesc(volRZ(:,:,slc));
figure; imagesc(RestVol(:,:,slc));
figure; imagesc(volRZ(:,:,slc)-RestVol(:,:,slc));


%%% Resizing the vol into three different resolutions and check whether
%%% They are consistent under slice interpolation.
load([data_path 'AnatData/simT2Reg2pat_brain.mat']);
vol = anatomicalT2_volume;
run([matlab_path 'DataInfo/DataInfo_Origin']);
dim_target1 = [128 128 62];
[vol1, DataInfo1] = volresize(vol, dim_target1, DataInfo);
dim_target2 = [128 128 31];
[vol2, DataInfo2] = volresize(vol, dim_target2, DataInfo);
dim_target3 = [256 256 31];
[vol3, DataInfo3] = volresize(vol, dim_target3, DataInfo);

% Set up the slice data info
run([matlab_path 'DataInfo/DataInfo_Sim']);
DataInfo1.min_ext_slices = DataInfo.min_ext_slices;
DataInfo1.max_ext_slices = DataInfo.max_ext_slices;
DataInfo1.dim_slices = DataInfo.dim;
DataInfo1.interp_order = 1;
DataInfo1.account_thickness = false;
DataInfo2.min_ext_slices = DataInfo.min_ext_slices;
DataInfo2.max_ext_slices = DataInfo.max_ext_slices;
DataInfo2.dim_slices = DataInfo.dim_slices;
DataInfo2.interp_order = 1;
DataInfo2.account_thickness = false;
DataInfo3.min_ext_slices = DataInfo.min_ext_slices;
DataInfo3.max_ext_slices = DataInfo.max_ext_slices;
DataInfo3.dim_slices = DataInfo.dim_slices;
DataInfo3.interp_order = 1;
DataInfo3.account_thickness = false;

% Interpolation
slice_z = 80;
DataInfo1.dim_slices(3) = 1;
DataInfo1.max_ext_slices(3) = slice_z;
DataInfo1.min_ext_slices(3) = slice_z;
DataInfo2.dim_slices(3) = 1;
DataInfo2.max_ext_slices(3) = slice_z;
DataInfo2.min_ext_slices(3) = slice_z;
DataInfo3.dim_slices(3) = 1;
DataInfo3.max_ext_slices(3) = slice_z;
DataInfo3.min_ext_slices(3) = slice_z;
angles = zeros(3,1);
xyz = zeros(3,1);
slc1=Vol2SliceInterpolation(vol1, 1, [angles(:); xyz(:)], DataInfo1);
figure; imagesc(slc1);
slc2=Vol2SliceInterpolation(vol2, 1, [angles(:); xyz(:)], DataInfo2);
figure; imagesc(slc2);
slc3=Vol2SliceInterpolation(vol3, 1, [angles(:); xyz(:)], DataInfo3);
figure; imagesc(slc3);
%%
TEST_END