run('../Initial.m');
TEST_BEGIN;
%%
clear;
run('../Initial.m');
result_file = [result_path 'MT_MI64_Exponential2_FminSimplexInitCtrlPts_SimDataLinearVol0to0_test/xt_map.mat'];
load(result_file);

CheckEstimationResult(xt_map);

CheckEstimationResult(xt_map, 1:14);

%
load([data_path 'SimulatedData/GroundTruth_old.mat']);
[RMSE]=CheckEstimationResult(xt_map, 1:size(xt_map,2), xt_truth);

RMSE
%%
TEST_END;
