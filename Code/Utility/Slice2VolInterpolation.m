function [vol, vol_count]=Slice2VolInterpolation(observed_slice, slc_idx, Pars, DataInfo)

% check field existance
if(~isfield(DataInfo, 'dim') || ~isfield(DataInfo, 'min_ext') ||...
        ~isfield(DataInfo, 'max_ext'))
    error('DataInfo must contains refernce vol coordinate info.');
else
    dim = DataInfo.dim;
    min_ext = DataInfo.min_ext;
    max_ext = DataInfo.max_ext;
end
if(~isfield(DataInfo, 'dim_slices') || ~isfield(DataInfo, 'min_ext_slices') ||...
        ~isfield(DataInfo, 'max_ext_slices'))
    error('DataInfo must contains slice coordinate info');
else
    dim_slices = DataInfo.dim_slices;
    min_ext_slices = DataInfo.min_ext_slices;
    max_ext_slices = DataInfo.max_ext_slices;
end
if(~isfield(DataInfo, 'account_thickness'))
    error('DataInfo must specifies the account_thickness field.');
else
    account_thickness = DataInfo.account_thickness;
end

% Check whether the precalculated information exists
if(~isfield(DataInfo, 'delta1'))
    % Prepare the transformed slice coordinate
    DataInfo = CalcCoordinateInfo(DataInfo);
end
delta1 = DataInfo.delta1;
delta2 = DataInfo.delta2;
delta3 = DataInfo.delta3;
line_1 = DataInfo.line_1;
line_2 = DataInfo.line_2;
slice_Z_values = DataInfo.slice_z_values;

    
% Calculate offset vector
if(account_thickness==false)
    z_offset = 0;
else
    thickness = (max_ext_slices(3)-min_ext_slices(3)) / (dim_slices(3)-1);
    halfnumslc = floor(thickness/(2*delta3));
    z_offset = -halfnumslc*delta3:delta3:halfnumslc*delta3;
end

% Compute the coordinate for each grid.
Num_of_input = numel(slc_idx);
GridCoordinates = zeros(3, dim_slices(1)*dim_slices(2)*numel(z_offset), Num_of_input);
for s = 1:Num_of_input
    z_cor = repmat(slice_Z_values(slc_idx(s))*ones(1,numel(z_offset))+z_offset(:)', [dim_slices(1)*dim_slices(2) 1]);
    xyz_cor = [repmat(line_1(:)', [1 numel(z_offset)]);
               repmat(line_2(:)', [1 numel(z_offset)]);
               z_cor(:)'];
    GridCoordinates(:,:,s) = xyz_cor;
end

angles = Pars(1:3);
xyz = Pars(4:6);
% Multiply out to speed up
cs=cos(angles*pi/180);
sn=sin(angles*pi/180);
A = [  cs(1)*cs(2)                     sn(1)*cs(2)                     -sn(2);
       cs(1)*sn(2)*sn(3)-sn(1)*cs(3)   sn(1)*sn(2)*sn(3)+cs(1)*cs(3)   sn(3)*cs(2);
       cs(1)*sn(2)*cs(3)+sn(1)*sn(3)   sn(1)*sn(2)*cs(3)-cs(1)*sn(3)   cs(3)*cs(2);];

% Perform the interpolation
vol=zeros(dim);
vol_count = zeros(dim);
for j=1:size(z_offset(:),1)
    rotated_volume = A*GridCoordinates(:,:,s) +...
        repmat(xyz(:),[1 size(GridCoordinates,2)]);  
    
    x_mat=(reshape(rotated_volume(1,:),[dim_slices(1) dim_slices(2) numel(z_offset)]) -...
        (min_ext(1)))/delta1+1;
    y_mat=(reshape(rotated_volume(2,:),[dim_slices(1) dim_slices(2) numel(z_offset)]) -...
        (min_ext(2)))/delta2+1;
    z_mat=(reshape(rotated_volume(3,:),[dim_slices(1) dim_slices(2) numel(z_offset)]) -...
        (min_ext(3)))/delta3+1;
    
    voxel1 = round(y_mat);
    voxel2 = round(x_mat);
    voxel3 = round(z_mat);
    valid = x_mat>=1 & y_mat>=1 & z_mat>=1 &...
        x_mat<=dim(2) & y_mat<=dim(1) & z_mat<=dim(3);
    vol(sub2ind(dim, voxel1(valid), voxel2(valid),voxel3(valid))) = observed_slice(valid);
    vol_count(sub2ind(dim, voxel1(valid), voxel2(valid),voxel3(valid))) = 1;
end

end