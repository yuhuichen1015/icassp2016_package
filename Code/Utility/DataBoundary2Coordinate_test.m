run('../Initial.m');
TEST_BEGIN;
%%
clear;
dim_1 = [256 256 124];
min_boundary = [0 0 -35];
max_boundary = [200 200 150];

[GridCoordinate1, min_ext_1, max_ext_1] = DataBoundary2Cooridnate(dim_1, min_boundary, max_boundary);


dim_2 = [128 128 64];
[GridCoordinate1, min_ext_2, max_ext_2] = DataBoundary2Cooridnate(dim_2, min_boundary, max_boundary);

[min_update_2, max_update_2] = ResizeDataCoordinate(dim_1, min_ext_1, max_ext_1, dim_2);
min_ext_2
min_update_2
max_ext_2
max_update_2

[min_update_1, max_update_1] = ResizeDataCoordinate(dim_2, min_ext_2, max_ext_2, dim_1);
min_ext_1
min_update_1
max_ext_1
max_update_1

%%
TEST_END;