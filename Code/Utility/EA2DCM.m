function [DCM]=EA2DCM(EA)

if(numel(EA)~=3)
    error('Dimension of Euler angles should be 3.');
end
cs=cos(EA*pi/180);
sn=sin(EA*pi/180);
DCM = [  cs(1)*cs(2)                     sn(1)*cs(2)                     -sn(2);
    cs(1)*sn(2)*sn(3)-sn(1)*cs(3)   sn(1)*sn(2)*sn(3)+cs(1)*cs(3)   sn(3)*cs(2);
    cs(1)*sn(2)*cs(3)+sn(1)*sn(3)   sn(1)*sn(2)*cs(3)-cs(1)*sn(3)   cs(3)*cs(2);];

end