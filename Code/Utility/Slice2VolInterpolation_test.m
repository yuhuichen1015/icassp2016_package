run('../Initial.m');
TEST_BEGIN;
%%

clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth_old.mat']);
% Load and add datainfo
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
% Add Data description:
DataInfo.activation_level = 5;
% Load the activation volumns.
load([data_path 'SimulatedData/PeriodVols_z31_' int2str(DataInfo.activation_level) '.mat']);
slices_sequence=[DataInfo.jmin:2:DataInfo.jmax DataInfo.jmin+1:2:DataInfo.jmax];

observed_slice = RestVol(:,:,15);
figure; imagesc(observed_slice);

k=15;
%observed_slice=imresize(data(:,:,j), [dim(1) dim(2)]);
xyz=xt_truth(4:6,k);
angles=xt_truth(1:3,k);
slc_idx = slices_sequence(mod(k-1,14)+1);

% interpolation order 0
DataInfo.account_thickness = false;
[vol, vol_count]=Slice2VolInterpolation(observed_slice, slc_idx, [angles(:); xyz(:)], DataInfo);
figure; imagesc(sum(vol,3));
figure; imagesc(sum(vol_count,3));

DataInfo.account_thickness = true;
[vol, vol_count]=Slice2VolInterpolation(observed_slice, slc_idx, [angles(:); xyz(:)], DataInfo);
figure; imagesc(sum(vol,3));
figure; imagesc(sum(vol_count,3));

DataInfo.interp_order = 0;
slc1=Vol2SliceInterpolation(vol, slc_idx, [angles(:); xyz(:)], DataInfo);
figure; imagesc(slc1);
figure; imagesc(slc1-observed_slice);

[vol2, vol_count2]=Slice2VolInterpolation(slc1, slc_idx, [angles(:); xyz(:)], DataInfo);
slc2=Vol2SliceInterpolation(vol2, slc_idx, [angles(:); xyz(:)], DataInfo);
slc_count2 = Vol2SliceInterpolation(vol_count2, slc_idx, [angles(:); xyz(:)], DataInfo);
figure; imagesc(slc1-slc2); % should be all zeros
figure; imagesc(slc_count2);

%%
TEST_END;