run('../Initial.m');
TEST_BEGIN;
%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth.mat']);

run([matlab_path 'DataInfo/DataInfo_Sim.m']);
DataInfo.slice_sequence = [1:2:13 2:2:14];
tic;
[AvgDp] = ShowAvgDisplacement(1:10, xt_truth_origin, xt_truth, DataInfo); % Should not be zero
mean(AvgDp(:))
toc
tic;
[AvgDp] = ShowAvgDisplacement(1:10, xt_truth, xt_truth, DataInfo) % Should be zero
mean(AvgDp(:))
toc

% Test for translation
tic;
[AvgDp]=ShowAvgDisplacement(1, [0;0;0;1;0;0], [0;0;0;0;0;0], DataInfo) % Should be 1
toc
tic;
[AvgDp]=ShowAvgDisplacement(1, [0;0;0;0;1;0], [0;0;0;0;0;0], DataInfo) % Should be 1
toc
tic;
AvgDp=ShowAvgDisplacement(1, [0;0;0;0;0;1], [0;0;0;0;0;0], DataInfo) % Should be 1
toc
tic;
AvgDp=ShowAvgDisplacement(1, [0;0;0;1;0;1], [0;0;0;0;0;0], DataInfo) % Should be 1.414
toc

tic;
AvgDp=ShowAvgDisplacement(1, [0;3;0;1;0;1], [0;0;0;0;0;0], DataInfo) 
toc

% Test for joing origin
tic;
DataInfo.joint_origin = Target_joint_origin;
AvgDp=ShowAvgDisplacement(1:1820, xt_truth_origin, xt_truth, DataInfo); % Should be zero
mean(AvgDp)
toc



%%
TEST_END;