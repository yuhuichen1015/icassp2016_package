run('../Initial.m');
TEST_BEGIN;
%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth_old.mat']);
% Load and add datainfo
% Add Data description:
activation_level = 5;
% Load the activation volumns.
load([data_path 'SimulatedData/PeriodVols_z31_' int2str(activation_level) '.mat']);
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
Vol2SlcInfo = struct();
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;
Vol2SlcInfo.jmin = DataInfo.jmin;
Vol2SlcInfo.jmax = DataInfo.jmax;
Vol2SlcInfo.dim_slices = DataInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = DataInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = DataInfo.max_ext_slices;

slices_sequence=[Vol2SlcInfo.jmin:2:Vol2SlcInfo.jmax...
    Vol2SlcInfo.jmin+1:2:Vol2SlcInfo.jmax];

% Compare nearest interpolation result
load([data_path 'SimulatedData/SimDataNearest/vol_0.mat']);
k=13;
xyz = xt_truth(4:6,k);
angles = xt_truth(1:3,k);
slc_idx = slices_sequence(mod(k-1,Vol2SlcInfo.dim_slices(3)) + 1);

Vol2SlcInfo.interp_order = 0;
Vol2SlcInfo.account_thickness = true;
tic;
observed_slice=Vol2SliceInterpolation(RestVol, slc_idx, [angles(:); xyz(:);], Vol2SlcInfo);
toc
figure; imagesc(observed_slice);
figure; imagesc(observed_slice-data(:,:,slc_idx)); % should be all zero

% Compare linear interpolation result
load([data_path 'SimulatedData/SimDataLinear/vol_0.mat']);
k=13;
xyz = xt_truth(4:6,k);
angles = xt_truth(1:3,k);
slc_idx = slices_sequence(mod(k-1,Vol2SlcInfo.dim_slices(3)) + 1);

Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
tic;
observed_slice=Vol2SliceInterpolation(RestVol, slc_idx, [angles(:); xyz(:);], Vol2SlcInfo);
toc
figure; imagesc(observed_slice);
figure; imagesc(observed_slice-data(:,:,slc_idx)); % should be all zero

%%
% Check interpolation under different resolution
load([data_path 'AnatData/SimT2Vol.mat']);
load([data_path 'SimulatedData/SimDataLinear/vol_0.mat']);
run([matlab_path 'DataInfo/DataInfo_Origin.m']);


Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

k=13;
xyz=xt_truth(4:6,k);
angles=xt_truth(1:3,k);
slc_idx = slices_sequence(mod(k-1,Vol2SlcInfo.dim_slices(3)) + 1);

% interpolation order 1
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
tic;
slc=Vol2SliceInterpolation(AnatVol, slc_idx, [angles(:); xyz(:)], Vol2SlcInfo);
toc
figure; imagesc(slc);
figure; imagesc(data(:,:,slc_idx));
figure; imagesc(slc-data(:,:,slc_idx));

Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = false;
tic;
slc=Vol2SliceInterpolation(AnatVol, slc_idx, [angles(:); xyz(:)], Vol2SlcInfo);
toc
figure; imagesc(slc);
figure; imagesc(data(:,:,slc_idx));
figure; imagesc(slc-data(:,:,slc_idx));

%% Check the a number of slices
% PartVol data generation
load([data_path 'AnatData/SimT1Vol.mat']);
k=10;
xyz=xt_truth(4:6,k);
angles=xt_truth(1:3,k);

Vol2SlcInfo.interp_order = 0;
Vol2SlcInfo.account_thickness = false;
slc_idx = 1:14;
PartVol = zeros(Vol2SlcInfo.dim_slices(1), Vol2SlcInfo.dim_slices(2), numel(slc_idx));

tic;
for s=1:numel(slc_idx)
    PartVol(:,:,s) = Vol2SliceInterpolation(AnatVol, slc_idx(s), [angles(:); xyz(:)], Vol2SlcInfo);
end
toc

tic;
ObservedSlices = Vol2SliceInterpolation(AnatVol, slc_idx, [angles(:); xyz(:)], Vol2SlcInfo);
toc
sum(abs(PartVol(:)-ObservedSlices(:)))

Vol2SlcInfo.interp_order = 0;
Vol2SlcInfo.account_thickness = true;
slc_idx = 7:9;
PartVol = zeros(Vol2SlcInfo.dim_slices(1), Vol2SlcInfo.dim_slices(2), numel(slc_idx));
for s=1:numel(slc_idx)
    PartVol(:,:,s) = Vol2SliceInterpolation(AnatVol, slc_idx(s), [angles(:); xyz(:)], Vol2SlcInfo);
end

ObservedSlices = Vol2SliceInterpolation(AnatVol, slc_idx, [angles(:); xyz(:)], Vol2SlcInfo);
sum(abs(PartVol(:)-ObservedSlices(:)))

Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = false;
slc_idx = 7:9;
PartVol = zeros(Vol2SlcInfo.dim_slices(1), Vol2SlcInfo.dim_slices(2), numel(slc_idx));
for s=1:numel(slc_idx)
    PartVol(:,:,s) = Vol2SliceInterpolation(AnatVol, slc_idx(s), [angles(:); xyz(:)], Vol2SlcInfo);
end

ObservedSlices = Vol2SliceInterpolation(AnatVol, slc_idx, [angles(:); xyz(:)], Vol2SlcInfo);
sum(abs(PartVol(:)-ObservedSlices(:)))

Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
slc_idx = 7:9;
PartVol = zeros(Vol2SlcInfo.dim_slices(1), Vol2SlcInfo.dim_slices(2), numel(slc_idx));
for s=1:numel(slc_idx)
    PartVol(:,:,s) = Vol2SliceInterpolation(AnatVol, slc_idx(s), [angles(:); xyz(:)], Vol2SlcInfo);
end

ObservedSlices = Vol2SliceInterpolation(AnatVol, slc_idx, [angles(:); xyz(:)], Vol2SlcInfo);
sum(abs(PartVol(:)-ObservedSlices(:)))


%% Test joint origin
k=10;
xyz=xt_truth(4:6,k);
angles=xt_truth(1:3,k);

Joint_origin = [100 100 100];
Vol2SlcInfo.joint_origin = Joint_origin;
[xt_global] = CalcGlobalPars(xt_truth(:,k), Vol2SlcInfo);

Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
slc_idx = 5;

Slc_nochange = Vol2SliceInterpolation(AnatVol, slc_idx, xt_truth(:,k), Vol2SlcInfo);
figure; imagesc(Slc_nochange);

Vol2SlcInfo.joint_origin = [0 0 0];
Slc_change = Vol2SliceInterpolation(AnatVol, slc_idx, xt_global, Vol2SlcInfo);
figure; imagesc(Slc_change);
figure; imagesc(Slc_nochange-Slc_change);


%%
% Precalculated coordinate info
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
% Prepare the transformed slice coordinate
line_d1 = linspace(Vol2SlcInfo.min_ext_slices(1),Vol2SlcInfo.max_ext_slices(1),Vol2SlcInfo.dim_slices(1));
line_d2 = linspace(Vol2SlcInfo.min_ext_slices(2),Vol2SlcInfo.max_ext_slices(2),Vol2SlcInfo.dim_slices(2));
[line_2, line_1] = meshgrid(line_d2,line_d1);
Vol2SlcInfo.line_1 = line_1;
Vol2SlcInfo.line_2 = line_2;

Vol2SlcInfo.delta1 = (Vol2SlcInfo.max_ext(1)-Vol2SlcInfo.min_ext(1)) / (Vol2SlcInfo.dim(1)-1);
Vol2SlcInfo.delta2 = (Vol2SlcInfo.max_ext(2)-Vol2SlcInfo.min_ext(2)) / (Vol2SlcInfo.dim(2)-1);
Vol2SlcInfo.delta3 = (Vol2SlcInfo.max_ext(3)-Vol2SlcInfo.min_ext(3)) / (Vol2SlcInfo.dim(3)-1);
Vol2SlcInfo.slice_z_values = linspace(Vol2SlcInfo.min_ext_slices(3),Vol2SlcInfo.max_ext_slices(3),Vol2SlcInfo.dim_slices(3));

k=10;
slc_idx = 3;
slc=Vol2SliceInterpolation(AnatVol, slc_idx, xt_truth(:,k), Vol2SlcInfo);
figure; imagesc(slc);
Vol2SlcInfo = rmfield(Vol2SlcInfo, 'delta1');
slc_idx = 3;
slc=Vol2SliceInterpolation(AnatVol, slc_idx, xt_truth(:,k), Vol2SlcInfo);
figure; imagesc(slc);

%% Test for contrl points inputs
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth_old.mat']);
% Load and add datainfo
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
% Add Data description:
Vol2SlcInfo = DataInfo;
Vol2SlcInfo.activation_level = 5;
% Load the activation volumns.
load([data_path 'SimulatedData/PeriodVols_z31_' int2str(Vol2SlcInfo.activation_level) '.mat']);

k=10;
xyz=xt_truth(4:6,k);
angles=xt_truth(1:3,k);
slc_idx = 5;

% interpolation order 0
Vol2SlcInfo.interp_order = 0;
Vol2SlcInfo.account_thickness = true;
slc1=Vol2SliceInterpolation(RestVol, slc_idx, xt_truth(:,k), Vol2SlcInfo);
figure; imagesc(slc1);

slice_z_values = linspace(Vol2SlcInfo.min_ext_slices(3),Vol2SlcInfo.max_ext_slices(3),Vol2SlcInfo.dim_slices(3));

slc2=Vol2SliceInterpolation(RestVol, slc_idx,...
    Pars2CtrlPts([angles(:); xyz(:)], Vol2SlcInfo.min_ext_slices, Vol2SlcInfo.max_ext_slices)...
    , Vol2SlcInfo);
figure; imagesc(slc1-slc2);


%% System offset 
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth_old.mat']);
% Load and add datainfo
% Add Data description:
% Load the activation volumns.
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
Vol2SlcInfo = struct();
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;
Vol2SlcInfo.jmin = DataInfo.jmin;
Vol2SlcInfo.jmax = DataInfo.jmax;
Vol2SlcInfo.dim_slices = DataInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = DataInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = DataInfo.max_ext_slices;
slices_sequence=[Vol2SlcInfo.jmin:2:Vol2SlcInfo.jmax...
    Vol2SlcInfo.jmin+1:2:Vol2SlcInfo.jmax];

run([matlab_path 'DataInfo/DataInfo_Origin.m']);
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

load([data_path 'AnatData/SimT1Vol.mat']);
load([data_path 'SimulatedData/SimDataLinear/vol_0.mat']);

k=13;
xyz=xt_truth(4:6,k);
angles=xt_truth(1:3,k);
slc_idx = slices_sequence(mod(k-1,Vol2SlcInfo.dim_slices(3)) + 1);

% interpolation order 1
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
tic;
slc=Vol2SliceInterpolation(AnatVol, slc_idx, [angles(:); xyz(:)], Vol2SlcInfo);
toc
figure; imagesc(slc);
figure; imagesc(data(:,:,slc_idx));

sysoffset_xyz = [-5, 10, -20];
sysoffset_angles = [5, 3, 5];
sysoffset_dcm = EA2DCM(sysoffset_angles);

tic;
slc1=Vol2SliceInterpolation(AnatVol, slc_idx, [sysoffset_angles(:); sysoffset_xyz(:)], Vol2SlcInfo);
toc
figure; imagesc(slc1);
figure; imagesc(data(:,:,slc_idx));

Vol2SlcInfo.sysoffset_xyz = sysoffset_xyz;
Vol2SlcInfo.sysoffset_angles = sysoffset_angles;
Vol2SlcInfo.sysoffset_dcm = sysoffset_dcm;
tic;
slc2=Vol2SliceInterpolation(AnatVol, slc_idx, zeros(6,1), Vol2SlcInfo);
toc
figure; imagesc(slc2);
figure; imagesc(data(:,:,slc_idx));
figure; imagesc(slc1-slc2);


%%
TEST_END;