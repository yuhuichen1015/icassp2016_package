function [PtsMtx]=Pars2CtrlPts(pars, min_slices, max_slices)

width = max(max_slices(1)-min_slices(1), max_slices(2)-min_slices(2));
Pts = width*0.5*[0 1 0; cos(pi/6) -sin(pi/6) 0; -cos(pi/6) -sin(pi/6) 0;]';

% Perform affine transformation
angles = pars(1:3);
trans = pars(4:6);
cs=cos(angles*pi/180);
sn=sin(angles*pi/180);
B=[1 0 0;0 cs(3) sn(3);0 -sn(3) cs(3)];
C=[cs(2) 0 -sn(2);0 1 0;sn(2) 0 cs(2)];
D=[cs(1) sn(1) 0;-sn(1) cs(1) 0;0 0 1];
A=B*C*D;
Pts = A*Pts;
PtsMtx = Pts + repmat(trans(:), [1,3]);

end