run('../Initial.m');
TEST_BEGIN;
%%

clear;
run('../Initial.m');

% Load and add datainfo
run([matlab_path 'DataInfo/DataInfo_Sim.m']);

[UpdateInfo] = CalcCoordinateInfo(DataInfo);
UpdateInfo

%%
TEST_END;