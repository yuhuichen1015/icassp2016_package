run('../Initial.m');
TEST_BEGIN;
%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth.mat']);

run([matlab_path 'DataInfo/DataInfo_Sim.m']);
DataInfo.slice_sequence = [1:2:13 2:2:14];

% Test for translation
tic;
[AvgDp]=CalcAvgDisplacement([0;0;0;1;0;0], [0;0;0;0;0;0], 1, DataInfo) % Should be 1
toc
tic;
[AvgDp]=CalcAvgDisplacement([0;0;0;0;1;0], [0;0;0;0;0;0], 1, DataInfo) % Should be 1
toc
tic;
AvgDp=CalcAvgDisplacement([0;0;0;0;0;1], [0;0;0;0;0;0], 1, DataInfo) % Should be 1
toc
tic;
AvgDp=CalcAvgDisplacement([0;0;0;1;0;1], [0;0;0;0;0;0], 1, DataInfo) % Should be 1.414
toc

tic;
AvgDp=CalcAvgDisplacement([0;3;0;1;0;1], [0;0;0;0;0;0], 1, DataInfo) 
toc

tic;
AvgDp=CalcAvgDisplacement([0;3;0;0;0;0], [0;0;0;0;0;0], 1, DataInfo) 
toc

%%
TEST_END;