run('../Initial.m');
TEST_BEGIN;
%% Check the global parameter calculation
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth_old.mat']);
% Load and add datainfo
% Add Data description:
% Load the activation volumns.
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
Vol2SlcInfo = struct();
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;
Vol2SlcInfo.jmin = DataInfo.jmin;
Vol2SlcInfo.jmax = DataInfo.jmax;
Vol2SlcInfo.dim_slices = DataInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = DataInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = DataInfo.max_ext_slices;
slices_sequence=[Vol2SlcInfo.jmin:2:Vol2SlcInfo.jmax...
    Vol2SlcInfo.jmin+1:2:Vol2SlcInfo.jmax];

run([matlab_path 'DataInfo/DataInfo_Origin.m']);
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

% Generate the moved volume
load([data_path 'AnatData/SimT2Vol.mat']);
Vol2SlcInfo.sysoffset_xyz = [5; -3; -5;];
Vol2SlcInfo.sysoffset_angles = [3; 5; -5;];
Vol2SlcInfo.sysoffset_dcm = EA2DCM(Vol2SlcInfo.sysoffset_angles);
Vol2SlcInfo.joint_origin = [10 20 30];
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;

xt = [-10; -10; 5; -3; -10; 15];
slices1=Vol2SliceInterpolation(AnatVol, 5, xt, Vol2SlcInfo);
figure; imagesc(slices1);

[xt_global] = CalcGlobalPars(xt, Vol2SlcInfo);
xt_global

Vol2SlcInfo=rmfield(Vol2SlcInfo, 'sysoffset_xyz');
Vol2SlcInfo=rmfield(Vol2SlcInfo, 'sysoffset_angles');
Vol2SlcInfo=rmfield(Vol2SlcInfo, 'sysoffset_dcm');
Vol2SlcInfo=rmfield(Vol2SlcInfo, 'joint_origin');

slices2=Vol2SliceInterpolation(AnatVol, 5, xt_global, Vol2SlcInfo);
figure; imagesc(slices2);
figure; imagesc(slices1-slices2); % should be all zeros;

%% Check the inverse calculation
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth_old.mat']);
% Load and add datainfo
% Add Data description:
% Load the activation volumns.
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
Vol2SlcInfo = struct();
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;
Vol2SlcInfo.jmin = DataInfo.jmin;
Vol2SlcInfo.jmax = DataInfo.jmax;
Vol2SlcInfo.dim_slices = DataInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = DataInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = DataInfo.max_ext_slices;
slices_sequence=[Vol2SlcInfo.jmin:2:Vol2SlcInfo.jmax...
    Vol2SlcInfo.jmin+1:2:Vol2SlcInfo.jmax];

run([matlab_path 'DataInfo/DataInfo_Origin.m']);
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

% Generate the moved volume
load([data_path 'AnatData/SimT2Vol.mat']);
Vol2SlcInfo.sysoffset_xyz = [5; -3; -5;];
Vol2SlcInfo.sysoffset_angles = [3; 5; -5;];
Vol2SlcInfo.sysoffset_dcm = EA2DCM(Vol2SlcInfo.sysoffset_angles);
Vol2SlcInfo.joint_origin = [10 20 30];
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;

xt = [-10; -10; 5; -3; -10; 15];
[xt_inv] = CalcGlobalPars(xt, Vol2SlcInfo, 1);

slices1=Vol2SliceInterpolation(AnatVol, 5, xt_inv, Vol2SlcInfo);
figure; imagesc(slices1);

Vol2SlcInfo=rmfield(Vol2SlcInfo, 'sysoffset_xyz');
Vol2SlcInfo=rmfield(Vol2SlcInfo, 'sysoffset_angles');
Vol2SlcInfo=rmfield(Vol2SlcInfo, 'sysoffset_dcm');
Vol2SlcInfo=rmfield(Vol2SlcInfo, 'joint_origin');

slices2=Vol2SliceInterpolation(AnatVol, 5, xt, Vol2SlcInfo);
figure; imagesc(slices2);
figure; imagesc(slices1-slices2); % should be all zeros

%%
TEST_END;