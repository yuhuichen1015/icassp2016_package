run('../Initial.m');
TEST_BEGIN;
%%

angles = [5 -15 -10];
EA2DCM(angles)
SpinCalc('EA321toDCM', angles, 0.001, 0)


tic;
for i=1:1000
    EA2DCM(angles);
end
toc

EA2DCM = inline('')

tic;
for i=1:1000
    cs=cos(angles*pi/180);
    sn=sin(angles*pi/180);
    Ab = [  cs(1)*cs(2)                     sn(1)*cs(2)                     -sn(2);
            cs(1)*sn(2)*sn(3)-sn(1)*cs(3)   sn(1)*sn(2)*sn(3)+cs(1)*cs(3)   sn(3)*cs(2);
            cs(1)*sn(2)*cs(3)+sn(1)*sn(3)   sn(1)*sn(2)*cs(3)-cs(1)*sn(3)   cs(3)*cs(2);];
end
toc

tic;i
for i=1:1000
    [Ac] = SpinCalc('EA321toDCM', angles, 0.001, 0);
end
toc

%%
TEST_END;