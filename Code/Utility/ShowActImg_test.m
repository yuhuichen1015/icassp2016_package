run('../Initial.m');
TEST_BEGIN;
%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([result_path 'GroundTruthPars/ReconstructPInfo.mat']);

[ActImgSep, ActImgFDR, ActImgSgl] = ShowActImg(15, 0.05, PInfo);
figure; 
subplot(1,3,1); image(ActImgSep), axis image;
subplot(1,3,2); image(ActImgFDR), axis image;
subplot(1,3,3); image(ActImgSgl), axis image;

[ActImgSep, ActImgFDR, ActImgSgl] = ShowActImg(20, 0.05, PInfo);
figure; 
subplot(1,3,1); image(ActImgSep), axis image;
subplot(1,3,2); image(ActImgFDR), axis image;
subplot(1,3,3); image(ActImgSgl), axis image;

%%
TEST_END;