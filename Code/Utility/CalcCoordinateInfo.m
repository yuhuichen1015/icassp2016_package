function [DataInfo] = CalcCoordinateInfo(DataInfo)

if(any(~isfield(DataInfo, {'min_ext_slices', 'max_ext_slices', 'dim_slices', ...
        'min_ext', 'max_ext', 'dim'})))
    DataInfo
    error('CalcCooridateInfo function requires the input struct has missing fields.');
end

% Prepare the transformed slice coordinate
line_d1 = linspace(DataInfo.min_ext_slices(1), DataInfo.max_ext_slices(1), DataInfo.dim_slices(1));
line_d2 = linspace(DataInfo.min_ext_slices(2), DataInfo.max_ext_slices(2), DataInfo.dim_slices(2));
DataInfo.slice_z_values = linspace(DataInfo.min_ext_slices(3), DataInfo.max_ext_slices(3), DataInfo.dim_slices(3));
[line_2, line_1] = meshgrid(line_d2,line_d1);
DataInfo.line_1 = line_1;
DataInfo.line_2 = line_2;

DataInfo.delta1 = (DataInfo.max_ext(1)-DataInfo.min_ext(1)) / (DataInfo.dim(1)-1);
DataInfo.delta2 = (DataInfo.max_ext(2)-DataInfo.min_ext(2)) / (DataInfo.dim(2)-1);
DataInfo.delta3 = (DataInfo.max_ext(3)-DataInfo.min_ext(3)) / (DataInfo.dim(3)-1);


end