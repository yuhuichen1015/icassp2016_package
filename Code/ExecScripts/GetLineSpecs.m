function LineSpecs = GetLineSpecs(Labels)

Cnt = 0;

Specs = {'-', '--', '-.', '.', '-x', '-o', '-*'};
LineSpecs = {};

for l = 1:numel(Labels)
    label = Labels{l};
    token = strtok(label, '_');
    switch(token)
        case 'GroundTruthPars'
            Cnt = Cnt+1;
            LineSpecs = [LineSpecs, ['r' Specs{Cnt}]];
        case 'NoCorrection'
            LineSpecs = [LineSpecs, ['k' Specs{Cnt}]];
        case 'V2V'
            Cnt = Cnt + 1;
            LineSpecs = [LineSpecs, ['g' Specs{Cnt}]];
        case 'MSV'
            Cnt = Cnt + 1;
            LineSpecs = [LineSpecs, ['m' Specs{Cnt}]];
        case 'MT'
            Cnt = Cnt + 1;
            LineSpecs = [LineSpecs, ['b' Specs{Cnt}]];
        otherwise
            error(['Unrecognized token:' token]);
    end
end

end