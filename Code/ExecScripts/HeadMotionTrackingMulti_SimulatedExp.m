clear;
run('../Initial.m');
PInfo = struct();

% Set up general parameters
PInfo.overwrite = 1;    % 0: don't overwrite.
                        % 1: start a new experiment to overwrite.
                        % 2: continue the previous unfinished work.
PInfo.preregistration = true;
PInfo.matlab_path = matlab_path;
PInfo.note_label = [''];
PInfo.HalfWindowSize = 2;

% Set up EPI data set
PInfo.dataset_name = 'LinearSysOffsetReal';
PInfo.dataset_path = [data_path 'SimulatedData/' PInfo.dataset_name '/'];
PInfo.volnum_start = 1;
PInfo.volnum_end = 130;
PInfo.slice_sequence = [1:2:14 2:2:14];
PInfo.dataset_label = [PInfo.dataset_name 'Vol' int2str(PInfo.volnum_start)...
    'to' int2str(PInfo.volnum_end)];

% 
PInfo.parsest_volnum_start = 1;
PInfo.parsest_volnum_end = 5;

% Set up ground truth file path
PInfo.ground_truth_filename = [PInfo.dataset_path 'TruthParsGlobal.mat'];

% Set up anatomical volume
PInfo.dataset_anat = [data_path 'AnatData/SimT1Vol.mat'];
PInfo.dataset_anatinfo = [matlab_path 'DataInfo/DataInfo_Origin.m'];

% Set up particle and parallel computing parameters
PInfo.num_of_processes = 4;
PInfo.num_of_particles = 4000;
PInfo.Gau_sigma = 0.1*eye(6);
PInfo.Beta = 0;
PInfo.missing_threshold = 0.2;

% Set up metric related parameters
PInfo.define_metric_script = [matlab_path 'SimilarityMetric/Define_MutualInformation.m'];
MetricInfo = struct();
MetricInfo.bin = 64;% Set up the metric label
MetricInfo.metric_label = ['MI' int2str(MetricInfo.bin)];
PInfo.metric_info = MetricInfo;

% Set up vol2slc metric
PInfo.define_vol2slc_metric_script = [matlab_path 'SimilarityMetric/Define_Vol2Slice_Metric.m'];
Vol2SlcInfo = struct();
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
PInfo.vol2slc_info = Vol2SlcInfo;

% Set up objective function
PInfo.define_optimization_script = [matlab_path 'Optimization/Define_Opt_Fmin_MeanRandParsInit_CtrlPts_Reg.m'];
OptimInfo = struct();
OptimInfo.max_iteration = 1000;
OptimInfo.tolerance_func = 10^(-5);
OptimInfo.space_dim = 9;
OptimInfo.num_of_trials = 5;
OptimInfo.rand_sigma = PInfo.Gau_sigma;
OptimInfo.optim_label = ['FminMeanRandParsInitCtrlptsReg'];
PInfo.optim_info = OptimInfo;

% Set up the output folder name;
PInfo.output_folder = [result_path 'MT_'...
    MetricInfo.metric_label '_'...
    OptimInfo.optim_label '_'...
    PInfo.dataset_label '_'...
    PInfo.note_label '/'];
%%         
HeadMotionTrackingMulti;

