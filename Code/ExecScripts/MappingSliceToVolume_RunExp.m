clear;
run('../Initial.m');
PInfo = struct();

% Set up general parameters
PInfo.overwrite = 1;    % 0: don't overwrite.
                        % 1: start a new experiment to overwrite.
                        % 2: continue the previous unfinished work.
PInfo.matlab_path = matlab_path;
PInfo.note_label = [''];


% Set up the path for v2v registration result for initialization.
PInfo.v2v_registered_folder = [result_path 'V2V_trials5_range3_Run1_CorVol1to126_/'];

% Set up EPI data set
PInfo.dataset_name = 'Run1_Cor';
PInfo.dataset_path = [data_path 'RealData/' PInfo.dataset_name '/'];
PInfo.volnum_start = 1;
PInfo.volnum_end = 126;
PInfo.slice_sequence = [1:2:14 2:2:14];
PInfo.dataset_label = [PInfo.dataset_name 'Vol' int2str(PInfo.volnum_start)...
    'to' int2str(PInfo.volnum_end)];

% Set up anatomical volume
PInfo.dataset_anat = [data_path 'AnatData/RunT1Vol.mat'];
PInfo.dataset_anatinfo = [matlab_path 'DataInfo/DataInfo_RunAnat.m'];

% Set up metric related parameters
PInfo.define_metric_script = [matlab_path 'SimilarityMetric/Define_MutualInformation.m'];
MetricInfo = struct();
MetricInfo.bin = 64;% Set up the metric label
MetricInfo.metric_label = ['MI' int2str(MetricInfo.bin)];
PInfo.metric_info = MetricInfo;

% Set up vol2slc metric
PInfo.define_vol2slc_metric_script = [matlab_path 'SimilarityMetric/Define_Vol2Slice_Metric.m'];
Vol2SlcInfo = struct();
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
PInfo.vol2slc_info = Vol2SlcInfo;

% Set up objective function
PInfo.define_optimization_script = [matlab_path 'Optimization/Define_Opt_Fmin_MeanRandParsInit_CtrlPts.m'];
OptimInfo = struct();
OptimInfo.rand_range = 3;
OptimInfo.num_of_trials = 5;
OptimInfo.tolerance_func = 10^-5;
OptimInfo.max_iteration = 1000;
OptimInfo.space_dim = 9;
OptimInfo.optim_label = ['FminMeanRandParsInitCtrlPts'];
PInfo.optim_info = OptimInfo;

PInfo.msv_label = ['trials' int2str(OptimInfo.num_of_trials) '_range' int2str(OptimInfo.rand_range)]


% Set up the output folder name;
PInfo.output_folder = [result_path 'MSV_'...
    PInfo.msv_label '_'...
    MetricInfo.metric_label '_'...
    OptimInfo.optim_label '_'...
    PInfo.dataset_label '_'...
    PInfo.note_label '/'];


%%         
MappingSliceToVolume;
