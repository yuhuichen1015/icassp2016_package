
ResultMap = containers.Map();
ResultMap('NoCorrection') = [result_path 'NoCorrection/'];
ResultMap('GroundTruthPars') = [result_path 'GroundTruthPars/'];

% V2V Results
ResultMap('V2V_Simulated') = [result_path 'V2V_trials5_range3_LinearSysOffsetRealVol1to130_/'];

% MSV Results
ResultMap('MSV_Simulated') = [result_path 'MSV_trials5_range3_MI64_FminMeanRandParsInitCtrlPts_LinearSysOffsetRealVol1to130_/'];

% MT Results
ResultMap('MT_Simulated') = [result_path 'MT_MI64_FminMeanRandParsInitCtrlptsReg_LinearSysOffsetRealVol1to130_/'];


% V2V Results
ResultMap('V2V_Run') = [result_path 'V2V_trials5_range3_Run1_CorVol1to126_/'];

% MSV Results
ResultMap('MSV_Run') = [result_path 'MSV_trials5_range3_MI64_FminMeanRandParsInitCtrlPts_Run1_CorVol1to126_/'];

% MT Results
ResultMap('MT_Run') = [result_path 'MT_MI64_FminMeanRandParsInitCtrlPtsReg_Run1_CorVol1to126_/'];