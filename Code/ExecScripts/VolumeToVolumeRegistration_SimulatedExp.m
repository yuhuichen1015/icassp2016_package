clear;
run('../Initial.m');
PInfo = struct();

% Set up general parameters
PInfo.overwrite = 1;    % 0: don't overwrite.
                        % 1: start a new experiment to overwrite.
                        % 2: continue the previous unfinished work.
PInfo.preregistration = false;
PInfo.matlab_path = matlab_path;
PInfo.note_label = [''];

% Set up EPI data set
PInfo.dataset_name = 'LinearSysOffsetReal';
PInfo.dataset_path = [data_path 'SimulatedData/' PInfo.dataset_name '/'];
PInfo.volnum_start = 1;
PInfo.volnum_end = 130;
PInfo.slice_sequence = [1:2:14 2:2:14];
PInfo.dataset_label = [PInfo.dataset_name 'Vol' int2str(PInfo.volnum_start)...
    'to' int2str(PInfo.volnum_end)];


PInfo.v2v_num_of_trials = 5;
PInfo.v2v_rand_range = 3;


% Set up ground truth file path
PInfo.ground_truth_filename = [PInfo.dataset_path 'TruthParsGlobal.mat'];

% Set up anatomical volume
PInfo.dataset_anat = [data_path 'AnatData/SimT1Vol.mat'];
PInfo.dataset_anatinfo = [matlab_path 'DataInfo/DataInfo_Origin.m'];

% Set up vol2slc metric
PInfo.define_vol2slc_metric_script = [matlab_path 'SimilarityMetric/Define_Vol2Slice_Metric.m'];
Vol2SlcInfo = struct();
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
PInfo.vol2slc_info = Vol2SlcInfo;

PInfo.v2v_label = ['trials' int2str(PInfo.v2v_num_of_trials) '_range' int2str(PInfo.v2v_rand_range)];

% Set up the output folder name;
PInfo.output_folder = [result_path 'V2V_'...
    PInfo.v2v_label '_'...
    PInfo.dataset_label '_'...
    PInfo.note_label '/'];
%%       
VolumeToVolumeRegistration;
