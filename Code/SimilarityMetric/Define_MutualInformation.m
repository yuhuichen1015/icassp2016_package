x_range = MetricInfo.x_range;
y_range = MetricInfo.y_range;
edges_X = linspace(x_range(1), x_range(2), MetricInfo.bin);
edges_Y = linspace(y_range(1), y_range(2), MetricInfo.bin);
Metric = @(X,Y) Metric_MutualInformation(X, Y, edges_X, edges_Y);

