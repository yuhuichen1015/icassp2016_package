% Prepare the transformed slice coordinate
Vol2SlcInfo = CalcCoordinateInfo(Vol2SlcInfo);

Vol2SlcMetric = @(xt, slc_idx, EPI_slc) Metric(Vol2SliceInterpolation(AnatVol, slc_idx, xt(:), Vol2SlcInfo),...
    EPI_slc);

% Set up volume to slice description.
Vol2SlcInfo.description = ['Use the motion parameter directly calculate the interpolated slice from volume.'];