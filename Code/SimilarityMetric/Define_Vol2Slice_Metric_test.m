run('../Initial.m');
TEST_BEGIN;
%%

clear;
run('../Initial.m');

Vol2SlcInfo = struct();
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
% Set up input EPI data
input_folder = [data_path 'SimulatedData/SimDataLinear/'];
load([input_folder 'DataInfo.mat']);
Vol2SlcInfo.dim_slices = DataInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = DataInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = DataInfo.max_ext_slices;

% Set up reference data
load([data_path 'AnatData/SimT1Vol.mat']);
run([matlab_path 'DataInfo/DataInfo_Origin.m']);
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

% Set up the image similarity measure
load([input_folder 'vol_0.mat']);
MetricInfo = struct();
MetricInfo.bin = 64;
MetricInfo.x_range = [min(AnatVol(:)) max(AnatVol(:))];
MetricInfo.y_range = [min(data(:)) max(data(:))];
run('Define_MutualInformation.m');

% Set up the volume to slice image similarity measure
run('./Define_Vol2Slice_Metric.m');

EPI_slc = data(:,:,1);
figure; imagesc(EPI_slc);
Vol2SlcMetric([0;0;0;0;0;0], 1, EPI_slc) % Should have the largest MI value
Vol2SlcMetric([0;0;0;10;0;0], 1, EPI_slc)
Vol2SlcMetric([0;0;15;0;0;0], 1, EPI_slc)

%%
TEST_END;