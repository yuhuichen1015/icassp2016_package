run('../Initial.m');
TEST_BEGIN;
%%

clear;
run('../Initial.m');
load([data_path 'AnatData/SimT1Vol.mat']);

MetricInfo = struct();
MetricInfo.bin = 64;
MetricInfo.x_range = [0 255];
MetricInfo.y_range = [0 255];
run('Define_NormalizedMutualInformation.m');

% Case 1: should be larger  
refIdx = 50;
movIdx = 50;
Metric(AnatVol(:,:,refIdx), AnatVol(:,:,movIdx))


% Case 2: should be smaller 
refIdx = 50;
movIdx = 35;
Metric(AnatVol(:,:,refIdx), AnatVol(:,:,movIdx))

%%
TEST_END;