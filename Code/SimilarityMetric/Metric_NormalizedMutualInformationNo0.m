function [NMI] = Metric_NormalizedMutualInformationNo0(X, Y, edges_X, edges_Y)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
edges_X(1) = edges_X(1)-eps; edges_X(end) = edges_X(end)+eps; 
edges_Y(1) = edges_Y(1)-eps; edges_Y(end) = edges_Y(end)+eps;
hst=hist2d([X(:) Y(:)],edges_X,edges_Y)+eps;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hst(1,1) = 0;
hst=hst/sum(hst(:));
hstY=sum(hst,1);
hstX=sum(hst,2);
%The weights propotional to MI
HXY = -sum(sum(hst.*log(hst++eps)));
HX = -sum(hstX.*log(hstX+eps));
HY = -sum(hstY.*log(hstY+eps));
NMI = (HX+HY) / HXY;

end