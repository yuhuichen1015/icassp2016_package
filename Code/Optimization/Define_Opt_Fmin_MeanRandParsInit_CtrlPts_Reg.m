% Calculate the weighted mean of xtmp
if(sum(pr_particles(:))>1+0.001 || sum(pr_particles(:))<1-0.001)
    error('The weights should be normalized to sum to 1');
end
xtmp_weighted = xtmp.*repmat(pr_particles(:)', [6,1]);
xinit = sum(xtmp_weighted,2);
OptimInfo.xinit = xinit;
OptimInfo.min_ext_slices = Vol2SlcInfo.min_ext_slices;
OptimInfo.max_ext_slices = Vol2SlcInfo.max_ext_slices;

% Define the objective function
if(time_idx<=2)
    ObjFunc = @(xt, slc_idx, EPI_slc) -Vol2SlcMetric(xt, slc_idx, EPI_slc);
else
    ObjFunc = @(xt, slc_idx, EPI_slc) -Vol2SlcMetric(xt, slc_idx, EPI_slc) + PInfo.Beta*norm(CtrlPts2Pars(reshape(xt,[3,3]))-2*xt_1+xt_2)^2;
end

%%%%%%%%%%%%%%%%%%%%%%%
xopt_cand = zeros(9, OptimInfo.num_of_trials);
fval_cand = zeros(1, OptimInfo.num_of_trials);
for trial = 1:OptimInfo.num_of_trials
    options = OptimInfo2options(OptimInfo);
    [x_optPts, f, exit_flag, output] = fminsearch_modified(...
        @(x) ObjFunc(x(:), epi_slc_idx, epi_slc)...
        , zeros(9,1), options);
    
    xopt_cand(:,trial) = x_optPts(:);
    fval_cand(trial) = ObjFunc(x_optPts(:), epi_slc_idx, epi_slc);
end
%%% Choose the best one
[yy, ind] = sort(fval_cand, 'ascend');
x_optPts = xopt_cand(:,ind(1));

x_opt = CtrlPts2Pars(reshape(x_optPts,[3,3]));
obj_val = fval_cand(ind(1));

%%%%%%%%%%%%%%%%%
OptimInfo.options = options;
OptimInfo.description = ['Use MATLAB fminesarch with weighted mean as initialization. Randomly generate the initial points around the mean control points.'];
