% Define the objective function
ObjFunc = @(xt, slc_idx, EPI_slc) -Vol2SlcMetric(xt, slc_idx, EPI_slc);
  
% Prepare for the initial points
[mx,ind_mx]=sort(pr_particles,'descend');

%%%%%%%%%%%%%%%%%
%Find the minimum MI
xtmp_rep=xtmp(:,ind_mx(1:7)).';
[ x_opt, n_feval, f_val ] = nelder_mead ( xtmp_rep, @(x) ObjFunc(x, epi_slc_idx, epi_slc), 0 );
OptimInfo.description = ['Using Nelder-Mead algorithm with largest weights particles as initialization.'];
