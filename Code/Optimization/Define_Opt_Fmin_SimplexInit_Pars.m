% Define the objective function
ObjFunc = @(xt, slc_idx, EPI_slc) -Vol2SlcMetric(xt, slc_idx, EPI_slc);
  
% Prepare for the initial points
if(sum(pr_particles(:))>1+0.001 || sum(pr_particles(:))<1-0.001)
    error('The weights should be normalized to sum to 1');
end
[mx,ind_mx]=sort(pr_particles,'descend');

%%%%%%%%%%%%%%%%%
%Find the minimum MI
OptimInfo.n_init = xtmp(:,ind_mx(1:7));
options = OptimInfo2options(OptimInfo);
[x_opt, f, exit_flag, output] = fminsearch_modified(@(x) ObjFunc(x, epi_slc_idx, epi_slc), zeros(6,1), options);

OptimInfo.options = options;
OptimInfo.description = ['Use MATLAB fminsearch with largest weights particles as initialization.'];
