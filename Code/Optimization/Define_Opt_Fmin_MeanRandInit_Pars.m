% Define the objective function
ObjFunc = @(xt, slc_idx, EPI_slc) -Vol2SlcMetric(xt, slc_idx, EPI_slc);
  
% Calculate the weighted mean of xtmp
if(sum(pr_particles(:))>1+0.001 || sum(pr_particles(:))<1-0.001)
    error('The weights should be normalized to sum to 1');
end
xtmp_weighted = xtmp.*repmat(pr_particles(:)', [6,1]);
xinit = sum(xtmp_weighted,2);
OptimInfo.xinit = xinit;

%%%%%%%%%%%%%%%%%%%%%%%
xopt_cand = zeros(6, OptimInfo.num_of_trials);
fval_cand = zeros(1, OptimInfo.num_of_trials);
for trial = 1:OptimInfo.num_of_trials
    options = OptimInfo2options(OptimInfo);
    [x_opt, f, exit_flag, output] = fminsearch_modified(@(x) ObjFunc(x, epi_slc_idx, epi_slc), xinit, options);
    xopt_cand(:,trial) = x_opt(:);
    fval_cand(trial) = ObjFunc(x_opt(:), epi_slc_idx, epi_slc);
end

%%% Choose the best one
[yy, ind] = sort(fval_cand, 'ascend');
x_opt = xopt_cand(:,ind(1));


%%%%%%%%%%%%%%%%%
OptimInfo.options = options;
OptimInfo.description = ['Use MATLAB fminesarch with weighted mean as initialization. Randomly generate the initial points around the mean.'];
