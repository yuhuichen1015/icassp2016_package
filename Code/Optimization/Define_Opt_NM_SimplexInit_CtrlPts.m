% Prepare for the initial points
[mx,ind_mx]=sort(pr_particles,'descend');

%%%%%%%%%%%%%%%%%
%Find the minimum MI
xtmp_rep=xtmp(:,ind_mx(1:10));
xtmpPts = zeros(9,10);
for p=1:10
    [PtsMtx] = Pars2CtrlPts(xtmp_rep(:,p), Vol2SlcInfo.min_ext_slices, Vol2SlcInfo.max_ext_slices);
    xtmpPts(:,p) = PtsMtx(:);
end

% Define the objective function
ObjFunc = @(xt, slc_idx, EPI_slc) -Vol2SlcMetric(xt, slc_idx, EPI_slc);

[ x_optPts, n_feval, f_val ] = nelder_mead ( xtmpPts', @(x) ObjFunc(x(:), epi_slc_idx, epi_slc), 0 );
x_opt = CtrlPts2Pars(reshape(x_optPts,[3,3]));

OptimInfo.description = ['Using Nelder-Mead algorithm with largest weights particles as initialization. Control points as optimization parameters'];
