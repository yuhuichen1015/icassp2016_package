run('../Initial.m');
TEST_BEGIN;
%%

clear;
run('../Initial.m');
PInfo = struct();

% Set up ground truth file path
PInfo.ground_truth_filename = [data_path 'SimulatedData/GroundTruth.mat'];

% Set up EPI data set
PInfo.dataset_name = 'Linear';
PInfo.dataset_path = [data_path 'SimulatedData/' PInfo.dataset_name '/'];
PInfo.volnum_start = 1;
PInfo.volnum_end = 5;
PInfo.slice_sequence = [1:2:14 2:2:14];
PInfo.dataset_label = ['vol' int2str(PInfo.volnum_start)...
    'to' int2str(PInfo.volnum_end)];

% Set up metric related parameters
PInfo.define_metric_script = [matlab_path 'SimilarityMetric/Define_MutualInformation.m'];
MetricInfo = struct();
MetricInfo.bin = 64;% Set up the metric label
MetricInfo.metric_label = ['MI' int2str(MetricInfo.bin)];
PInfo.metric_info = MetricInfo;

% Set up vol2slc metric
PInfo.define_vol2slc_metric_script = [matlab_path 'SimilarityMetric/Define_Vol2Slice_Metric.m'];
Vol2SlcInfo = struct();
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
PInfo.vol2slc_info = Vol2SlcInfo;

% Set up input EPI data
input_folder = PInfo.dataset_path;
load([input_folder 'GenerationInfo.mat']);
Vol2SlcInfo.dim_slices = GenerationInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = GenerationInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = GenerationInfo.max_ext_slices;
Vol2SlcInfo.slice_Z_values=linspace(GenerationInfo.min_ext_slices(3),GenerationInfo.max_ext_slices(3),GenerationInfo.dim_slices(3));
Vol2SlcInfo.slice_sequence = GenerationInfo.slice_sequence;


% Set up anatomical volume
PInfo.dataset_anat = [data_path 'AnatData/SimT1Vol.mat'];
PInfo.dataset_anatinfo = [matlab_path 'DataInfo/DataInfo_Origin.m'];

% Set up reference data
load(PInfo.dataset_anat);
run(PInfo.dataset_anatinfo);

Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

% Set up the image similarity measure
load([input_folder 'vol_' int2str(PInfo.volnum_start) '.mat']);
MetricInfo = PInfo.metric_info;
MetricInfo.x_range = [min(AnatVol(:)), max(AnatVol(:))];
MetricInfo.y_range = [min(data(:)), max(data(:))];
run(PInfo.define_metric_script);
PInfo.metric_info = MetricInfo;  % Update the metric info.

% Set up the volume to slice image similarity measure
run(PInfo.define_vol2slc_metric_script);
PInfo.vol2slc_info = Vol2SlcInfo;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(PInfo.ground_truth_filename);
idx = 15;
volnum = floor(idx/14)+1;
slc = mod(idx-1,14)+1;
load([input_folder 'vol_' num2str(volnum) '.mat']);
['Processing volume:' int2str(volnum) ' slice:' int2str(slc)]
epi_slc = data(:,:,slc);
epi_slc_idx = slc;        


% Should be better than previous method
% xtmp = mvnrnd(xt_truth(:,idx), 1*eye(6),7)';
xtmp = repmat(zeros(6,1), [1,7]);
pr_particles = 1/7*ones(1,7);


OptimInfo.num_of_trials = 1;
OptimInfo.rand_range = 5;
OptimInfo.space_dim = 6;
OptimInfo.max_iteration = 1000;
OptimInfo.tolerance_func = 10^(-4);
% Calling NM algorithm
tic;
run([matlab_path 'Optimization/Define_Opt_Fmin_MeanRandInit_Pars.m']);
t=toc;
['Spent ' num2str(t) ' seconds']

xt_truth(:,idx)
x_opt(:)
mean(abs(xt_truth(:,idx)-x_opt(:)))

OptimInfo.num_of_trials = 3;
OptimInfo.rand_range = 5;
OptimInfo.space_dim = 6;
OptimInfo.max_iteration = 1000;
OptimInfo.tolerance_func = 10^(-4);
% Calling NM algorithm
tic;
run([matlab_path 'Optimization/Define_Opt_Fmin_MeanRandInit_Pars.m']);
t=toc;
['Spent ' num2str(t) ' seconds']

xt_truth(:,idx)
x_opt(:)
mean(abs(xt_truth(:,idx)-x_opt(:)))


%%
TEST_END;
