% Prepare for the initial points
if(sum(pr_particles(:))>1+0.001 || sum(pr_particles(:))<1-0.001)
    error('The weights should be normalized to sum to 1');
end
[mx,ind_mx]=sort(pr_particles,'descend');

%%%%%%%%%%%%%%%%%
%Find the minimum MI
xtmp_rep=xtmp(:,ind_mx(1:10));
xtmpPts = zeros(9,10);
for p=1:10
    [PtsMtx] = Pars2CtrlPts(xtmp_rep(:,p), Vol2SlcInfo.min_ext_slices, Vol2SlcInfo.max_ext_slices);
    xtmpPts(:,p) = PtsMtx(:);
end
OptimInfo.n_init = xtmpPts;

% Define the objective function
ObjFunc = @(xt, slc_idx, EPI_slc) -Vol2SlcMetric(xt, slc_idx, EPI_slc);

options = OptimInfo2options(OptimInfo);
[x_optPts, f, exit_flag, output] =fminsearch_modified(@(x) ObjFunc(x(:), epi_slc_idx, epi_slc), ones(9,1), options);
x_opt = CtrlPts2Pars(reshape(x_optPts,[3,3]));

OptimInfo.options = options;
OptimInfo.description = ['Using fminsearch with largest weights particles as initialization. Control points as optimization parameters'];
