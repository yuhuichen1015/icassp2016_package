clear;
run('../Initial.m');

run([matlab_path 'DataInfo/DataInfo_RunAnat.m']);
Vol2SlcInfo = struct();
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

load([data_path 'RealData/Run2_noskull/GenerationInfo.mat']);
Vol2SlcInfo.jmin = GenerationInfo.jmin;
Vol2SlcInfo.jmax = GenerationInfo.jmax;
Vol2SlcInfo.dim_slices = GenerationInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = GenerationInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = GenerationInfo.max_ext_slices;
slices_sequence=[Vol2SlcInfo.jmin:2:Vol2SlcInfo.jmax...
    Vol2SlcInfo.jmin+1:2:Vol2SlcInfo.jmax];

%% Before registration
load([data_path 'AnatData/RunT1Vol.mat']);
load([data_path 'RealData/Run2_noskull/vol_1.mat']);
Vol2SlcInfo.v2v_num_of_trials = 5;
Vol2SlcInfo.v2v_rand_range = 5;
Vol2SlcInfo.account_thickness = false;
Vol2SlcInfo.interp_order = 1;
slc1=Vol2SliceInterpolation(AnatVol, 1, zeros(6,1), Vol2SlcInfo);
figure; 
subplot(1,2,1); imagesc(data(:,:,1)), axis image;
subplot(1,2,2); imagesc(slc1), axis image;
slc11=Vol2SliceInterpolation(AnatVol, 11, zeros(6,1), Vol2SlcInfo);
figure;
subplot(1,2,1); imagesc(data(:,:,11)), axis image;
subplot(1,2,2); imagesc(slc11), axis image;

%%
% [EstPars]=RegisterVol2Vol(AnatVol, data, Vol2SlcInfo);
EstPars = [2.2577; -11.1916; -1.2365; -4.0024; -6.4197; 1.5183];
slc1=Vol2SliceInterpolation(AnatVol, 1, EstPars, Vol2SlcInfo);
figure; 
subplot(1,2,1); imagesc(data(:,:,1)), axis image;
subplot(1,2,2); imagesc(slc1), axis image;
slc11=Vol2SliceInterpolation(AnatVol, 11, EstPars, Vol2SlcInfo);
figure;
subplot(1,2,1); imagesc(data(:,:,11)), axis image;
subplot(1,2,2); imagesc(slc11), axis image;

%% Modify the coordinate
Vol2SlcInfo.max_ext_slices(3) = Vol2SlcInfo.min_ext_slices(3) + 6*13;
Vol2SlcInfo = CalcCoordinateInfo(Vol2SlcInfo);
slc1=Vol2SliceInterpolation(AnatVol, 1, zeros(6,1), Vol2SlcInfo);
figure; 
subplot(1,2,1); imagesc(data(:,:,1)), axis image;
subplot(1,2,2); imagesc(slc1), axis image;
slc11=Vol2SliceInterpolation(AnatVol, 11, zeros(6,1), Vol2SlcInfo);
figure;
subplot(1,2,1); imagesc(data(:,:,11)), axis image;
subplot(1,2,2); imagesc(slc11), axis image;

% [EstPars]=RegisterVol2Vol(AnatVol, data, Vol2SlcInfo);
EstPars = [1.7707; -11.4930; -0.7178; -4.5948; -7.2029; 0.3093;];
slc1=Vol2SliceInterpolation(AnatVol, 1, EstPars, Vol2SlcInfo);
figure; 
subplot(1,2,1); imagesc(data(:,:,1)), axis image;
subplot(1,2,2); imagesc(slc1), axis image;
slc11=Vol2SliceInterpolation(AnatVol, 11, EstPars, Vol2SlcInfo);
figure;
subplot(1,2,1); imagesc(data(:,:,11)), axis image;
subplot(1,2,2); imagesc(slc11), axis image;