clear;
run('../Initial.m');
dataset = 'Run3';
dataset_path = [data_path 'RealData/' dataset '/'];

output_path = [data_path 'RealData/' dataset '_noskull/'];
mkdir(output_path);
for volnum=1:126
    load([dataset_path 'vol_' int2str(volnum) '.mat']);
    thresh = 5000;
    datatmp = zeros(size(data));
    volnum
    for slc=1:14
        image = data(:,:,slc);
        % Threshold the image
        bwmap = image > thresh;
        
        % Select the main area
        CC = bwconncomp(bwmap, 6);
        numPixels = cellfun(@numel, CC.PixelIdxList);
        if(numel(CC.PixelIdxList)==0)
            continue;
        end
        [yy, idx] = sort(numPixels, 'descend');
        mainAreaIdx = CC.PixelIdxList{idx(1)};
        mainmap = zeros(size(bwmap));
        mainmap(mainAreaIdx) = 1;
%         figure; imagesc(mainmap);
        
        % Fill up holes
        mainfilled = imfill(mainmap);
%         figure; imagesc(mainfilled);
        
        % Dilate the image
        se = strel('disk', 1);
        mainSelect = imdilate(mainfilled, se);
        image(mainSelect(:)==0) = 0;
%         figure; imagesc(image);
        
        datatmp(:,:,slc) = image;
    end
    data = datatmp;
    save([output_path 'vol_' int2str(volnum) '.mat'], 'data');
end