
% Set up input EPI data
input_folder = PInfo.dataset_path;
load([input_folder 'GenerationInfo.mat']);
dim_slices = GenerationInfo.dim_slices;
min_ext_slices = GenerationInfo.min_ext_slices;
max_ext_slices = GenerationInfo.max_ext_slices;
slice_Z_values=linspace(min_ext_slices(3),max_ext_slices(3),dim_slices(3));
slice_sequence = GenerationInfo.slice_sequence;

Vol2SlcInfo = PInfo.vol2slc_info;
Vol2SlcInfo.dim_slices = GenerationInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = GenerationInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = GenerationInfo.max_ext_slices;

% Set up reference data
load(PInfo.dataset_anat);
run(PInfo.dataset_anatinfo);

Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

%%
if(isfield(PInfo, 'v2v_registered_folder'))
    load([PInfo.v2v_registered_folder 'xt_volume.mat']);
    V2VInfo = load([PInfo.v2v_registered_folder 'PInfo.mat'], 'PInfo');
    
    xt_init = CalcGlobalPars(xt_volume, V2VInfo.PInfo.vol2slc_info);
else
    % Estimate the system offset
    Vol2SlcInfo.v2v_num_of_trials = 10;
    Vol2SlcInfo.v2v_rand_range = 5;
    PInfo.vol2slc_info = Vol2SlcInfo;
    tic;
    [sysoffset_pars] = EstimateSysOffsetPars(AnatVol, PInfo);
    t=toc;
    ['Took ' num2str(t) ' seconds to estimate the system offset parameters.']
    xt_init = repmat(sysoffset_pars(:), [1 PInfo.volnum_end-PInfo.volnum_start+1]);
end
%% Remove this after its done
% xt_init = zeros(6,PInfo.volnum_end-PInfo.volnum_start+1);
%%

% Set up the image similarity measure
load([input_folder 'vol_' int2str(PInfo.volnum_start) '.mat']);
MetricInfo = PInfo.metric_info;
MetricInfo.x_range = [min(AnatVol(:)), max(AnatVol(:))];
MetricInfo.y_range = [min(data(:)), max(data(:))];
run(PInfo.define_metric_script);
PInfo.metric_info = MetricInfo;  % Update the metric info.

% Set up the volume to slice image similarity measure
run(PInfo.define_vol2slc_metric_script);
PInfo.vol2slc_info = Vol2SlcInfo;

% Create result folder
mkdir(PInfo.output_folder);
isexist = exist([PInfo.output_folder 'xt_map.mat'], 'file');
if(isexist && PInfo.overwrite==0)
    error('Result file exists. Not overwriting. Abort');
elseif(isexist && PInfo.overwrite==2)
    warning('Result file exists. Continuing previous status...');
else
    warning('Starting a new experiment....');
    % Set up the result data containers.
    xt_map=zeros(6, (PInfo.volnum_end-PInfo.volnum_start+1)*numel(slice_sequence));
    f_value=zeros(1,(PInfo.volnum_end-PInfo.volnum_start+1)*numel(slice_sequence));
    idx = 1;
    
    % Save the PInfo
    PInfo
    save([PInfo.output_folder 'PInfo.mat'], 'PInfo');
end

if(isfield(PInfo, 'ground_truth_filename'))
    load(PInfo.ground_truth_filename);
    f_opt = zeros(1,(PInfo.volnum_end-PInfo.volnum_start)*numel(slice_sequence));
end

%%% Start the multimodal tracking process
for volnum=PInfo.volnum_start:PInfo.volnum_end
    load([input_folder 'vol_' num2str(volnum) '.mat']);
    % Preprocess the data volume:
    % Loop through each slice
    for epi_slc_idx=PInfo.slice_sequence
        ['Processing volume:' int2str(volnum) ' slice:' int2str(epi_slc_idx)]
        epi_slc = data(:,:,epi_slc_idx);        
        
        %%%%%%%%%%%%%%%%%%%%%%%            
        % Check whether the epi slice contains enough information to be
        % used
        Missing_threshold = 0.15;
        if(sum(epi_slc(:)>10)/numel(epi_slc) > Missing_threshold)
            xtmp = xt_init(:,volnum);
            xtmp
            pr_particles = 1;
            OptimInfo = PInfo.optim_info;
            run(PInfo.define_optimization_script);
            PInfo.optim_info = OptimInfo;
        else % Missing observation
            x_opt = xt_init(:,volnum);
        end
        
        %%%%%%%%%%%%%%%%%
        % Collecting the result
        xt_map(:, idx) = x_opt(:);
        f_value(idx) = ObjFunc(x_opt(:), epi_slc_idx, epi_slc);
        x_opt
        if(isfield(PInfo, 'ground_truth_filename'))
            %             f_opt(idx) = ObjFunc(xt_global(:,idx), epi_z_position, epi_slc);
            xt_global(:,idx)
            mean(abs(xt_map(:,idx)-xt_global(:,idx)))
            save([PInfo.output_folder 'xt_map.mat'], 'idx', 'xt_map', 'f_value');
        else
            save([PInfo.output_folder 'xt_map.mat'], 'idx', 'xt_map', 'f_value');
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        idx=idx+1;
        %%%%%%%%%%%%%%%%%%%%%%%%%%
    end      
end
