clear;
Initial;
% Set line specs
set(0, 'DefaultAxesLineWidth', 2.0);
set(0, 'DefaultLineLineWidth', 2.0);
set(0, 'DefaultTextFontSize', 15);
set(0, 'DefaultTextFontWeight', 'bold');
set(0, 'DefaultAxesFontSize', 18);
set(0, 'DefaultAxesFontWeight', 'bold');
set(0, 'DefaultLineMarkerSize', 10);

run([matlab_path 'ExecScripts/Define_ResultMap.m']);

keys(ResultMap)'

%% Run1 Estimation 
SelectKeys = {'MSV_Run', 'MT_Run'};
Labels = {'MSV', 'HMT'};
k=1:200; % All
showPars = true;
for mtd = 1:numel(SelectKeys);
    h=figure;
    
    % Draw the method result
    result_folder = ResultMap(SelectKeys{mtd});
    load([result_folder 'PInfo.mat']);
    load([result_folder 'xt_map.mat']);
    [xt_show] = CalcGlobalPars(xt_map, PInfo.vol2slc_info);
    % Draw the rotation angles
    hold on;
    plot(k, xt_show(1,k), 'LineStyle', '-.', 'Color', [1 0 0], 'LineWidth', 4);
    plot(k, xt_show(2,k), 'LineStyle', '-.', 'Color', [0 1 0], 'LineWidth', 4);
    plot(k, xt_show(3,k), 'LineStyle', '-.', 'Color', [0 0 1], 'LineWidth', 4);
    title('Estimated rotation'),...
        axis([min(k(:)) max(k(:)) -25 25]);
    xlabel('time t');
    ylabel('degree');
    l=legend('$\hat\alpha$', '$\hat\beta$', '$\hat\gamma$', 'Location', 'northeast');
    set(l, 'Interpreter', 'latex');
    
     % Draw the V2V result
    result_folder = ResultMap('V2V_Run');
    load([result_folder 'PInfo.mat']);
    load([result_folder 'xt_map.mat']);
    [xt_show] = CalcGlobalPars(xt_map, PInfo.vol2slc_info);
    % Draw the rotation angles
    hold on;
    plot(k, xt_show(1,k), 'LineStyle', '-', 'Color', [0 0 0], 'LineWidth', 3);
    plot(k, xt_show(2,k), 'LineStyle', '-', 'Color', [0 0 0], 'LineWidth', 3);
    plot(k, xt_show(3,k), 'LineStyle', '-', 'Color', [0 0 0], 'LineWidth', 3);
    title('Estimated rotation'),...
        axis([min(k(:)) max(k(:)) -25 25]);
    xlabel('time t');
    ylabel('degree');
    l=legend('$\hat\alpha$', '$\hat\beta$', '$\hat\gamma$', 'Location', 'northwest');
    set(l, 'Interpreter', 'latex');
    hold on;

end

