
DataInfo = struct();
%%% Specify the coordinate and volume numbers
DataInfo.dim = [256 256 124];     			% dimension 
DataInfo.min_ext=[0.000000 0.000000 -36.640202]; 			% coordinate space extent
DataInfo.max_ext=[199.218750 199.218750 147.860001]; 			% coordinate space extent