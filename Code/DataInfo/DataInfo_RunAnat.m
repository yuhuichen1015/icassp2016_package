% Raw infomation
dim_raw = [256 256 120];
min_ext_raw = [-120.000000 -120.000000 -87.900002];
max_ext_raw = [119.062500 119.062500 90.599998];
line_1 = linspace(min_ext_raw(1), max_ext_raw(1), dim_raw(1));
line_2 = linspace(min_ext_raw(2), max_ext_raw(2), dim_raw(2));
line_3 = linspace(min_ext_raw(3), max_ext_raw(3), dim_raw(3));
select_min = [37 27 43];
select_max = [220 230 113];


DataInfo = struct();
%%% Specify the coordinate and volume numbers
DataInfo.dim = [184 204 71];     			% dimension 
DataInfo.min_ext=[line_1(select_min(1)) line_2(select_min(2)) line_3(select_min(3))]; 			% coordinate space extent
DataInfo.max_ext=[line_1(select_max(1)) line_2(select_max(2)) line_3(select_max(3))]; 			% coordinate space extent

% % Check
% d1 = (DataInfo.max_ext(1)-DataInfo.min_ext(1)) / (DataInfo.dim(1)-1)
% d2 = (DataInfo.max_ext(2)-DataInfo.min_ext(2)) / (DataInfo.dim(2)-1)
% d3 = (DataInfo.max_ext(3)-DataInfo.min_ext(3)) / (DataInfo.dim(3)-1)
