
DataInfo_Origin;
% Sub-sampled reference volume
dim_target=[128 128 31];
[min_update, max_update] = ResizeDataCoordinate(DataInfo.dim, DataInfo.min_ext, DataInfo.max_ext, dim_target);
DataInfo.dim = dim_target;
DataInfo.min_ext = min_update;
DataInfo.max_ext = max_update;

% EPI slices
EPI_z_range = [24.859800 102.860001];
DataInfo.jmin=1;
DataInfo.jmax=14;
DataInfo.dim_slices = [128 128 14];
min_ext_slices = DataInfo.min_ext;
min_ext_slices(3) = EPI_z_range(1);
DataInfo.min_ext_slices = min_ext_slices;
max_ext_slices = DataInfo.max_ext;
max_ext_slices(3) = EPI_z_range(2);
DataInfo.max_ext_slices = max_ext_slices;
