clear;
run(['Initial.m']);
% Set line specs
set(0, 'DefaultAxesLineWidth', 2.0);
set(0, 'DefaultLineLineWidth', 2.0);
set(0, 'DefaultTextFontSize', 15);
set(0, 'DefaultTextFontWeight', 'bold');
set(0, 'DefaultAxesFontSize', 18);
set(0, 'DefaultAxesFontWeight', 'bold');
set(0, 'DefaultLineMarkerSize', 10);

run([matlab_path 'ExecScripts/Define_ResultMap.m']);

keys(ResultMap)'

%% Show Estimated Pars (MSV)
SelectKeys = {'V2V_Simulated','MSV_Simulated', 'MT_Simulated'};
Labels = {'V2V', 'MSV', 'HMT'};

k=1:200; % All
showPars = true;

for mtd = 1:numel(SelectKeys)
    % Show ground truth
    result_folder = ResultMap(SelectKeys{mtd});
    load([result_folder 'PInfo.mat']);
    load([PInfo.dataset_path 'TruthParsGlobal.mat']);
    figure;
    
    % Show MSV
    load([result_folder 'xt_map.mat']);
    
    [xt_show] = CalcGlobalPars(xt_map, PInfo.vol2slc_info);
    hold on;
    plot(k, xt_show(1,k), 'LineStyle', '-.', 'Color', [1 0 0], 'LineWidth', 4);
    plot(k, xt_show(2,k), 'LineStyle', '-.', 'Color', [0 1 0], 'LineWidth', 4);
    plot(k, xt_show(3,k), 'LineStyle', '-.', 'Color', [0 0 1], 'LineWidth', 4);
    title(strcat('Estimated rotation:', Labels{mtd}));
    xlabel('time t');
    ylabel('degree'),
    axis([min(k(:)) max(k(:)) -10 10]);
    l=legend('$\hat\alpha$', '$\hat\beta$', '$\hat\gamma$');
    set(l, 'Interpreter', 'latex');
    
    % Show Groundtruth
    hold on;
    plot(k, xt_global(1,k), 'k-', ...
        k, xt_global(2,k), 'k-', ...
        k, xt_global(3,k), 'k-', 'LineWidth', 3)
    hold off; 
end


