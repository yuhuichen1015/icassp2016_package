
clear;
run('../Initial.m');

% Load and resize the anatomical volume
load([data_path 'AnatData/SimT2Vol.mat']);
load([result_path 'GroundTruthPars/PInfo.mat']);
Slc2VolInfo = GetSlc2VolInfo(PInfo);

Vol2SlcValidInfo.dim = PInfo.vol2slc_info.dim;
Vol2SlcValidInfo.min_ext = PInfo.vol2slc_info.min_ext;
Vol2SlcValidInfo.max_ext = PInfo.vol2slc_info.max_ext;
Vol2SlcValidInfo.dim_slices = [Slc2VolInfo.dim(2) Slc2VolInfo.dim(1) Slc2VolInfo.dim(3)];
Vol2SlcValidInfo.min_ext_slices = [Slc2VolInfo.min_ext(2) Slc2VolInfo.min_ext(1) Slc2VolInfo.min_ext(3)];
Vol2SlcValidInfo.max_ext_slices = [Slc2VolInfo.max_ext(2) Slc2VolInfo.max_ext(1) Slc2VolInfo.max_ext(3)];
Vol2SlcValidInfo.interp_order = 1;
Vol2SlcValidInfo.account_thickness = 1;
[tmpAnat] = Vol2SliceInterpolation(AnatVol, 1:Vol2SlcValidInfo.dim_slices(3), zeros(6,1), Vol2SlcValidInfo);
AnatVolRZ = zeros(Slc2VolInfo.dim);
for i=1:Slc2VolInfo.dim(3)
    AnatVolRZ(:,:,i) = tmpAnat(:,:,i)';
end

% Load the activation truth map
load([data_path 'AnatData/SimActTruth.mat']);
RestVol = AnatVolRZ;
ExecVol = AnatVolRZ + 0.05*(AnatVolRZ.*ActTruth);
save([data_path 'SimulatedData/PeriodVols_z31_5.mat'], 'RestVol', 'ExecVol');

% Load the activation truth map upper brain
load([data_path 'AnatData/SimActTruth_upper.mat']);
RestVol = AnatVolRZ;
ExecVol = AnatVolRZ + 0.05*(AnatVolRZ.*ActTruth);
save([data_path 'SimulatedData/PeriodVols_z31_5_upper.mat'], 'RestVol', 'ExecVol');

% Load the activation truth map lower brain
load([data_path 'AnatData/SimActTruth_lower.mat']);
RestVol = AnatVolRZ;
ExecVol = AnatVolRZ + 0.05*(AnatVolRZ.*ActTruth);
save([data_path 'SimulatedData/PeriodVols_z31_5_lower.mat'], 'RestVol', 'ExecVol');