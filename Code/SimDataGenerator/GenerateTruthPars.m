clear;
run('../Initial');
load([data_path 'SimulatedData/GroundTruth_old.mat']);
k=1:200;
CheckEstimationResult(xt_truth,k);

Target_joint_origin = [90 100 50];
PInfo.sysoffset_angles = [0 0 0];
PInfo.sysoffset_xyz = [0 0 0];
PInfo.sysoffset_dcm = eye(3);
% Estimate the origin
[origin]=EstimateOrigin(xt_truth, PInfo, 1)

% Calculate the origin removed parameters, and generate new parameters.
PInfo.joint_origin = origin;
[xt_est] = CalcGlobalPars(xt_truth, PInfo, 1);
CheckEstimationResult(xt_est,k);
xt_xyz = xt_est(4:6,:);
xt_xyz = xt_xyz - repmat(mean(xt_xyz,2), [1 size(xt_xyz,2)]);
xt_truth_origin = xt_truth;
xt_truth_origin(4:6,:) = 0.5*xt_xyz;
CheckEstimationResult(xt_truth,k);

% Calculate the parameters with Target origin.
PInfo.joint_origin = Target_joint_origin;
[xt_truth] = CalcGlobalPars(xt_truth_origin, PInfo,0);
CheckEstimationResult(xt_truth,k);

[origin]=EstimateOrigin(xt_truth, PInfo, 0)

save([data_path 'SimulatedData/GroundTruth.mat'], 'xt_truth', 'Target_joint_origin', 'xt_truth_origin');
