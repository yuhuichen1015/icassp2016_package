% Generate slices for registration
clear;
run('../Initial.m');

dataset_name = ['LinearSysOffsetReal'];
data_folder = [data_path 'SimulatedData/' dataset_name '/'];
mkdir(data_folder);

% Load and add datainfo
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
% Add Data description:
GenerationInfo = DataInfo;
run([matlab_path 'DataInfo/DataInfo_Origin.m']);
GenerationInfo.dim = DataInfo.dim;
GenerationInfo.min_ext = DataInfo.min_ext;
GenerationInfo.max_ext = DataInfo.max_ext;
GenerationInfo.interp_order = 1;
GenerationInfo.account_thickness = true;
GenerationInfo.description = ['Generate the data with given transformation parameters. Interpolation order:' int2str(GenerationInfo.interp_order) '.'];
GenerationInfo.slice_sequence = [GenerationInfo.jmin:2:GenerationInfo.jmax...
    GenerationInfo.jmin+1:2:GenerationInfo.jmax];

% Generate
% Load the ground truth parameters
load([data_path 'AnatData/SimT1Vol.mat']);
RefVol = AnatVol;
load([data_path 'AnatData/SimT2Vol.mat']);
load([data_path 'SimulatedData/GroundTruth.mat']);
GenerationInfo.joint_origin = zeros(3,1);
GenerationInfo.sysoffset_angles = [3; 5; -4];
GenerationInfo.sysoffset_xyz = [-9; 15; -7];
% GenerationInfo.sysoffset_angles = zeros(3,1);
% GenerationInfo.sysoffset_xyz = zeros(3,1);
GenerationInfo.sysoffset_dcm = EA2DCM(GenerationInfo.sysoffset_angles);

save([data_folder 'GenerationInfo.mat'], 'GenerationInfo');

%%
% fval_truth = zeros(130*14,1);
% k=1;
% for ktmp=1:130
%     data = zeros(GenerationInfo.dim_slices);
%     for slc_idx=GenerationInfo.slice_sequence      
%         ['Generating volume:' int2str(ktmp) ' slice:' int2str(slc_idx)]
%         slice=Vol2SliceInterpolation(AnatVol, slc_idx, xt_truth(:,k), GenerationInfo);
%         if(slc_idx <= 10)
%             %%% Modify the slice
%             slice = 256*imnoise(slice/256, 'gaussian', 0, 0.03^2);
%             blurKernel = fspecial('gaussian', [5 5], 2);
%             slice = imfilter(slice, blurKernel, 'same');
%         else
%             slice = zeros([GenerationInfo.dim_slices(1) GenerationInfo.dim_slices(2)]);
%         end
%         
%         sliceRef=Vol2SliceInterpolation(RefVol, slc_idx, xt_truth(:,k), GenerationInfo);
%         data(:,:,slc_idx) = slice;
%         [MI] = Metric_MutualInformation(slice, sliceRef, linspace(0,255,64), linspace(0,255,64));
%         fval_truth(k) = -MI;
%         
%         k = k + 1;
%     end
%     save([data_folder 'vol_' int2str(ktmp) '.mat'],'data'); 
% end

%% Generate slices for reconstruction
data_folder = [data_path 'SimulatedData/' dataset_name '_Rec/'];
mkdir(data_folder);

ReconstructInfo = GenerationInfo;
ReconstructInfo.interp_order = 0;
ReconstructInfo.account_thickness = false;
ReconstructInfo.activation_level = 5;
ReconstructInfo.z_dim = 31;
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
ReconstructInfo.dim = DataInfo.dim;
ReconstructInfo.min_ext = DataInfo.min_ext;
ReconstructInfo.max_ext = DataInfo.max_ext;

% Load the activation volumns.
load([data_path 'SimulatedData/PeriodVols_z' int2str(ReconstructInfo.z_dim)...
    '_' int2str(ReconstructInfo.activation_level) '.mat']);

save([data_folder 'ReconstructInfo.mat'], 'ReconstructInfo');
k=1;
for ktmp=1:130
    data = zeros(ReconstructInfo.dim_slices);
    for slc_idx=ReconstructInfo.slice_sequence
        if(mod(floor((ktmp-1)/10),2)==0)
            slice=Vol2SliceInterpolation(RestVol, slc_idx, xt_truth(:,k), ReconstructInfo);
        else   
            slice=Vol2SliceInterpolation(ExecVol, slc_idx, xt_truth(:,k), ReconstructInfo);
        end    
        data(:,:,slc_idx) = slice;
%         figure; imagesc(slice);
        k = k + 1;
    end
    save([data_folder 'vol_' int2str(ktmp) '.mat'],'data'); 
end

%% Generate slices for reconstruction (upper brain)
data_folder = [data_path 'SimulatedData/' dataset_name '_Rec_upper/'];
mkdir(data_folder);

ReconstructInfo = GenerationInfo;
ReconstructInfo.interp_order = 0;
ReconstructInfo.account_thickness = false;
ReconstructInfo.activation_level = 5;
ReconstructInfo.z_dim = 31;
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
ReconstructInfo.dim = DataInfo.dim;
ReconstructInfo.min_ext = DataInfo.min_ext;
ReconstructInfo.max_ext = DataInfo.max_ext;

% Load the activation volumns.
load([data_path 'SimulatedData/PeriodVols_z' int2str(ReconstructInfo.z_dim)...
    '_' int2str(ReconstructInfo.activation_level) '_upper.mat']);

save([data_folder 'ReconstructInfo.mat'], 'ReconstructInfo');
k=1;
for ktmp=1:130
    data = zeros(ReconstructInfo.dim_slices);
    for slc_idx=ReconstructInfo.slice_sequence
        if(mod(floor((ktmp-1)/10),2)==0)
            slice=Vol2SliceInterpolation(RestVol, slc_idx, xt_truth(:,k), ReconstructInfo);
        else   
            slice=Vol2SliceInterpolation(ExecVol, slc_idx, xt_truth(:,k), ReconstructInfo);
        end    
        data(:,:,slc_idx) = slice;
%         figure; imagesc(slice);
        k = k + 1;
    end
    save([data_folder 'vol_' int2str(ktmp) '.mat'],'data'); 
end

%% Generate slices for reconstruction (lower brain)
data_folder = [data_path 'SimulatedData/' dataset_name '_Rec_lower/'];
mkdir(data_folder);

ReconstructInfo = GenerationInfo;
ReconstructInfo.interp_order = 0;
ReconstructInfo.account_thickness = false;
ReconstructInfo.activation_level = 5;
ReconstructInfo.z_dim = 31;
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
ReconstructInfo.dim = DataInfo.dim;
ReconstructInfo.min_ext = DataInfo.min_ext;
ReconstructInfo.max_ext = DataInfo.max_ext;

% Load the activation volumns.
load([data_path 'SimulatedData/PeriodVols_z' int2str(ReconstructInfo.z_dim)...
    '_' int2str(ReconstructInfo.activation_level) '_lower.mat']);

save([data_folder 'ReconstructInfo.mat'], 'ReconstructInfo');
k=1;
for ktmp=1:130
    data = zeros(ReconstructInfo.dim_slices);
    for slc_idx=ReconstructInfo.slice_sequence
        if(mod(floor((ktmp-1)/10),2)==0)
            slice=Vol2SliceInterpolation(RestVol, slc_idx, xt_truth(:,k), ReconstructInfo);
        else   
            slice=Vol2SliceInterpolation(ExecVol, slc_idx, xt_truth(:,k), ReconstructInfo);
        end    
        data(:,:,slc_idx) = slice;
%         figure; imagesc(slice);
        k = k + 1;
    end
    save([data_folder 'vol_' int2str(ktmp) '.mat'],'data'); 
end
