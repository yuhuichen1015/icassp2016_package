clear;
run('../Initial.m');

% Load and resize the anatomical volume
load([data_path 'AnatData/SimT2Vol.mat']);
run([matlab_path 'DataInfo/DataInfo_Origin.m']);
dim_target = [128 128 31];
[VolRZT2, DataInfoRZT1] = volresize(AnatVol, dim_target, DataInfo);

load([data_path 'AnatData/SimT1Vol.mat']);
run([matlab_path 'DataInfo/DataInfo_Origin.m']);
dim_target = [128 128 31];
[VolRZT1, DataInfoRZT2] = volresize(AnatVol, dim_target, DataInfo);

% make sure the activation region are in valid volume
threshold = 30
VolBinary = VolRZT1 > threshold & VolRZT2 > threshold;

load([data_path 'AnatData/act_truth_odd.mat']);
ActTruth = VolBinary > 0 & act_truth > 0;
save([data_path 'AnatData/SimActTruth.mat'], 'ActTruth');

% Upper act truth
ActTruth_Upper = ActTruth;
ActTruth_Lower = ActTruth;
ActTruth_Upper(:,:,1:20) = 0;
ActTruth_Lower(:,:,21:end) = 0;

ActTruth = ActTruth_Upper;
save([data_path 'AnatData/SimActTruth_upper.mat'], 'ActTruth');

ActTruth = ActTruth_Lower;
save([data_path 'AnatData/SimActTruth_lower.mat'], 'ActTruth');