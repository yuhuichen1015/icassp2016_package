clear;
run('../Initial.m');

% Load and resize the anatomical volume
load([data_path 'AnatData/SimT2Vol.mat']);
run([matlab_path 'DataInfo/DataInfo_Origin.m']);
dim_target = [128 128 31];
[VolRZT2, DataInfoRZT1] = volresize(AnatVol, dim_target, DataInfo);
VolBin = VolRZT2 > 0;
VolInd = find(VolBin(:)==1);
[VolSub1, VolSub2,VolSub3] = ind2sub(dim_target, VolInd);
VolBoundary = boundary([VolSub1 VolSub2 VolSub3]);


%% Draw act truth
% set up act truth volume
actvolume_filename = [data_path 'AnatData/SimActTruth.mat'];

figure;
fv.Vertices = [VolSub1 VolSub2 VolSub3];
fv.Faces = VolBoundary;
p = patch(fv);
reducepatch(p, 0.05);
p.FaceColor = 'none';
p.EdgeColor = 'k';
p.LineWidth = 0.2;
p.LineStyle = '-';
daspect([1,1,1])
view(3); axis normal

load(actvolume_filename);
Labels = bwlabeln(ActTruth);
for l = 1:max(Labels(:))
    [sub1, sub2, sub3] = ind2sub(dim_target, find(Labels==l));
    regionBoundary = boundary([sub1 sub2 sub3]);
    hold on;
    trisurf(regionBoundary, sub1, sub2, sub3, 'FaceColor', 'red', 'EdgeColor', 'none');
end

%% Draw act truth upper
% set up act truth volume
actvolume_filename = [data_path 'AnatData/SimActTruth_upper.mat'];

figure;
fv.Vertices = [VolSub1 VolSub2 VolSub3];
fv.Faces = VolBoundary;
p = patch(fv);
reducepatch(p, 0.05);
p.FaceColor = 'none';
p.EdgeColor = 'k';
p.LineWidth = 0.2;
p.LineStyle = '-';
daspect([1,1,1])
view(3); axis normal

load(actvolume_filename);
Labels = bwlabeln(ActTruth);
for l = 1:max(Labels(:))
    [sub1, sub2, sub3] = ind2sub(dim_target, find(Labels==l));
    regionBoundary = boundary([sub1 sub2 sub3]);
    hold on;
    trisurf(regionBoundary, sub1, sub2, sub3, 'FaceColor', 'red', 'EdgeColor', 'none'), axis off;
end

%% Draw act truth upper
% set up act truth volume
actvolume_filename = [data_path 'AnatData/SimActTruth_lower.mat'];

figure;
fv.Vertices = [VolSub1 VolSub2 VolSub3];
fv.Faces = VolBoundary;
p = patch(fv);
reducepatch(p, 0.05);
p.FaceColor = 'none';
p.EdgeColor = 'k';
p.LineWidth = 0.2;
p.LineStyle = '-';
daspect([1,1,1])
view(3); axis normal

load(actvolume_filename);
Labels = bwlabeln(ActTruth);
for l = 1:max(Labels(:))
    [sub1, sub2, sub3] = ind2sub(dim_target, find(Labels==l));
    regionBoundary = boundary([sub1 sub2 sub3]);
    hold on;
    trisurf(regionBoundary, sub1, sub2, sub3, 'FaceColor', 'red', 'EdgeColor', 'none'), axis off;
end






