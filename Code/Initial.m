
WORKING_DIR = pwd;
matlab_path = [WORKING_DIR '/'];
result_path = [WORKING_DIR '/../Result/'];
data_path = [WORKING_DIR '/../Data/'];

addpath([matlab_path 'Utility/']);
% addpath([matlab_path 'TestUtil/']);
addpath([matlab_path 'SimilarityMetric/']);
addpath([matlab_path 'Optimization/']);
addpath([matlab_path 'MTalgorithm/']);
% addpath([matlab_path 'ActivationTest/']);
addpath([matlab_path 'Vol2VolRegistration/']);
addpath([matlab_path 'MSV/']);

