run('../Initial.m');
TEST_BEGIN;
%%

run('../Initial.m');
PInfo = struct();
% Set up EPI data set
PInfo.dataset_name = 'Linear';
PInfo.dataset_path = [data_path 'SimulatedData/' PInfo.dataset_name '/'];
PInfo.slice_sequence = [1:2:14 2:2:14];

% Set up anatomical volume
PInfo.dataset_anat = [data_path 'AnatData/SimT1Vol.mat'];
PInfo.dataset_anatinfo = [matlab_path 'DataInfo/DataInfo_Origin.m'];

% Set up metric related parameters
PInfo.define_metric_script = [matlab_path 'SimilarityMetric/Define_MutualInformation.m'];
MetricInfo = struct();
MetricInfo.bin = 64;% Set up the metric label
MetricInfo.metric_label = ['MI' int2str(MetricInfo.bin)];
PInfo.metric_info = MetricInfo;

% Set up vol2slc metric
PInfo.define_vol2slc_metric_script = [matlab_path 'SimilarityMetric/Define_Vol2Slice_Metric.m'];
Vol2SlcInfo = struct();
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
PInfo.vol2slc_info = Vol2SlcInfo;


%%
% Set up input EPI data
input_folder = PInfo.dataset_path;
load([input_folder 'GenerationInfo.mat']);
dim_slices = GenerationInfo.dim_slices;
min_ext_slices = GenerationInfo.min_ext_slices;
max_ext_slices = GenerationInfo.max_ext_slices;
slice_sequence = GenerationInfo.slice_sequence;

Vol2SlcInfo = PInfo.vol2slc_info;
Vol2SlcInfo.dim_slices = GenerationInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = GenerationInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = GenerationInfo.max_ext_slices;

% Set up reference data
load(PInfo.dataset_anat);
run(PInfo.dataset_anatinfo);

Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

% Set up the image similarity measure
load([input_folder 'vol_' int2str(1) '.mat']);
MetricInfo = PInfo.metric_info;
MetricInfo.x_range = [min(AnatVol(:)), max(AnatVol(:))];
MetricInfo.y_range = [min(data(:)), max(data(:))];
run(PInfo.define_metric_script);
PInfo.metric_info = MetricInfo;  % Update the metric info.

% Set up the volume to slice image similarity measure
run(PInfo.define_vol2slc_metric_script);
PInfo.vol2slc_info = Vol2SlcInfo;

%%
% Create result folder
init_pars = zeros(6,1);
Num_of_particles = 4000;
x0=repmat(init_pars(1:3),[1 Num_of_particles])+...
    randn(3,Num_of_particles)*sqrt(0.1);
x1=repmat(init_pars(4:6),[1 Num_of_particles])+...
    randn(3,Num_of_particles)*sqrt(0.1);
x_particles=[x0;x1];
%%
%%% Start the multimodal tracking process
volnum = 1;
epi_slc_idx = 1;
load([input_folder 'vol_' num2str(volnum) '.mat']);

['Processing volume:' int2str(volnum) ' slice:' int2str(epi_slc_idx)]
epi_slc = data(:,:,epi_slc_idx);

% add the gaussian noise
xtmp0=x_particles(1:3,:)+...
    diag(sqrt(0.1)*ones(3,1))*randn(size(x_particles(1:3,:)));
xtmp1=x_particles(4:6,:)+...
    diag(sqrt(0.1)*ones(3,1))*randn(size(x_particles(4:6,:)));
xtmp=[xtmp0;xtmp1];
        
        
%%%%%%%%%%%%%%%%%%%%%%%
pr_particles=zeros(Num_of_particles,1);
tic;
for i=1:Num_of_particles
    %%% Model is here
    pr_particles(i) = exp(Vol2SlcMetric(xtmp(:,i), epi_slc_idx, epi_slc)^2)-1;
end
toc;
pr_particles_origin=pr_particles/sum(pr_particles);  
figure; hist(pr_particles_origin,100);

%%%
dim = 6;
sigma = 8*randn(6,6);
sigma = sigma*sigma';
X=mvnrnd(zeros(1,dim), sigma, 4000);
Y=mvnpdf(X, zeros(1,dim), sigma);
Y = (Y-min(Y(:))) / (max(Y(:))-min(Y(:)));
Y = Y/sum(Y(:));
figure; hist(Y, 100);
       
%%%
[pr_particles]=EvalParticles(@(x) Vol2SlcMetric(x, epi_slc_idx, epi_slc), xtmp);
figure; hist(pr_particles,100);

%%
TEST_END;