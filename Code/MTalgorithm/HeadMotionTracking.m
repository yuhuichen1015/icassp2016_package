%%
% Set up input EPI data
input_folder = PInfo.dataset_path;
load([input_folder 'GenerationInfo.mat']);
dim_slices = GenerationInfo.dim_slices;
min_ext_slices = GenerationInfo.min_ext_slices;
max_ext_slices = GenerationInfo.max_ext_slices;
slice_sequence = GenerationInfo.slice_sequence;

Vol2SlcInfo = PInfo.vol2slc_info;
Vol2SlcInfo.dim_slices = GenerationInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = GenerationInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = GenerationInfo.max_ext_slices;

% Set up reference data
load(PInfo.dataset_anat);
run(PInfo.dataset_anatinfo);

Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

% Set up the image similarity measure
load([input_folder 'vol_' int2str(PInfo.volnum_start) '.mat']);
MetricInfo = PInfo.metric_info;
MetricInfo.x_range = [min(AnatVol(:)), max(AnatVol(:))];
MetricInfo.y_range = [min(data(:)), max(data(:))];
run(PInfo.define_metric_script);
PInfo.metric_info = MetricInfo;  % Update the metric info.
PInfo.Metric = Metric;

% Estimate the system offest
mkdir(PInfo.output_folder);
if(PInfo.preregistration)
    if(exist([PInfo.output_folder 'sysoffset_pars.mat'], 'file'))
        load([PInfo.output_folder 'sysoffset_pars.mat']);
    else
        Vol2SlcInfo.v2v_num_of_trials = 10;
        Vol2SlcInfo.v2v_rand_range = 5;
        PInfo.vol2slc_info = Vol2SlcInfo;
        tic;
        [sysoffset_pars] = EstimateSysOffsetPars(AnatVol, PInfo);
        t=toc;
        ['Took ' num2str(t) ' seconds to estimate the system offset parameters.']
        save([PInfo.output_folder 'sysoffset_pars.mat'], 'sysoffset_pars');
    end
    Vol2SlcInfo.sysoffset_angles = sysoffset_pars(1:3);
    Vol2SlcInfo.sysoffset_xyz = sysoffset_pars(4:6);
    Vol2SlcInfo.sysoffset_dcm = EA2DCM(sysoffset_pars(1:3));
    PInfo.vol2slc_info = Vol2SlcInfo;
    
    % Pre-register the first slice
    if(exist([PInfo.output_folder 'init_pars.mat'], 'file'))
        load([PInfo.output_folder 'init_pars.mat']);
    else
        tic;
        [init_pars, init_slc] = EstimateInitPars(AnatVol, PInfo);
        t=toc;
        if(strcmp(getComputerName(), 'schubert.eecs.umich.edu'))
            figure; imagesc(init_slc);
            figure; imagesc(data(:,:,1));
        end
        CalcGlobalPars(init_pars, Vol2SlcInfo)
        ['Took ' num2str(t) ' seconds to estimate the system offset parameters.']
        save([PInfo.output_folder 'init_pars.mat'], 'init_pars');
    end
else
    warning('Use default initial parameters');
    init_pars = zeros(6,1);
end
run(PInfo.define_vol2slc_metric_script);
PInfo.vol2slc_info = Vol2SlcInfo;
PInfo.Vol2SlcMetric = Vol2SlcMetric;

%% Estimate the model parameters
[joint_origin, Gau_sigma, Beta]=EstimateModelPars(init_pars, AnatVol, PInfo);
PInfo.Gau_sigma = Gau_sigma;
PInfo.Beta = Beta;
['Estimated Beta:' num2str(PInfo.Beta]
Vol2SlcInfo.joint_origin = joint_origin;
% Update the vol2slcmetric again
run(PInfo.define_vol2slc_metric_script);
PInfo.vol2slc_info = Vol2SlcInfo;
PInfo.Vol2SlcMetric = Vol2SlcMetric;

%% Save the PInfo
PInfo
save([PInfo.output_folder 'PInfo.mat'], 'PInfo');
if(isfield(PInfo, 'ground_truth_filename'))
    load(PInfo.ground_truth_filename);
    f_opt = zeros(1,(PInfo.volnum_end-PInfo.volnum_start)*numel(slice_sequence));
end

%% Start the estimation process.
EstimationLoop(PInfo.volnum_start, PInfo.volnum_end, init_pars, AnatVol, PInfo, 'xt_map.mat');

