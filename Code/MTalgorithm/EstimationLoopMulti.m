function EstimationLoopMulti(start_vol, end_vol, init_pars, AnatVol, PInfo, output_filename)

Vol2SlcMetric = PInfo.Vol2SlcMetric;
Vol2SlcInfo = PInfo.vol2slc_info;
% Set up the result data containers.
xt_map=zeros(6, (end_vol-start_vol+1)*numel(PInfo.slice_sequence));
xt_mean = zeros(6, (end_vol-start_vol+1)*numel(PInfo.slice_sequence));
covar=zeros(6, (end_vol-start_vol+1)*numel(PInfo.slice_sequence));
f_value=zeros(1, (end_vol-start_vol+1)*numel(PInfo.slice_sequence));

% Prepare initial particles
x_particles = repmat(init_pars(:), [1 PInfo.num_of_particles]);
%%% Start the multimodal tracking process
for volnum=start_vol:end_vol
    load([PInfo.dataset_path 'vol_' num2str(volnum) '.mat']);
    % Loop through each slice
    for slc_idx=1:numel(PInfo.slice_sequence)
        time_idx = (volnum-start_vol)*numel(PInfo.slice_sequence) + slc_idx;
       
        epi_slc_idx = [];
        epi_slc = [];
        
        Missing_threshold = PInfo.missing_threshold;
        HalfWindowSize = PInfo.HalfWindowSize;
%         %% Adding previous slice
%         if(slc_idx~=1)
%             pre_epi_slc_idx = PInfo.slice_sequence(slc_idx-1);
%             pre_epi_slc = data(:,:,pre_epi_slc_idx);
%             if(sum(pre_epi_slc(:))>10/numel(pre_epi_slc) > Missing_threshold) % check validity
%                 epi_slc_idx = [epi_slc_idx pre_epi_slc_idx];
%                 epi_slc = cat(3, epi_slc, pre_epi_slc);
%             end
%         end
%         %% Adding current slice
%         cur_epi_slc_idx = PInfo.slice_sequence(slc_idx);
%         cur_epi_slc = data(:,:,cur_epi_slc_idx);
%         epi_slc_idx = [epi_slc_idx cur_epi_slc_idx];
%         epi_slc = cat(3, epi_slc, cur_epi_slc);
%         %% Adding next slice
%         if(slc_idx~=numel(PInfo.slice_sequence))
%             next_epi_slc_idx = PInfo.slice_sequence(slc_idx+1);
%             next_epi_slc = data(:,:,next_epi_slc_idx);
%             if(sum(next_epi_slc(:))>10/numel(next_epi_slc) > Missing_threshold) % check validity
%                 epi_slc_idx = [epi_slc_idx next_epi_slc_idx];
%                 epi_slc = cat(3, epi_slc, next_epi_slc);
%             end
%         end
        for selectedIdx=slc_idx-HalfWindowSize:slc_idx+HalfWindowSize
            if(selectedIdx>=1 && selectedIdx<=max(PInfo.slice_sequence))
                this_epi_slc_idx = PInfo.slice_sequence(selectedIdx);
                this_epi_slc = data(:,:,this_epi_slc_idx);
                if(sum(this_epi_slc(:)>10)/numel(this_epi_slc) > Missing_threshold) % Check validity
                    epi_slc_idx = [epi_slc_idx this_epi_slc_idx];
                    epi_slc = cat(3, epi_slc, this_epi_slc);
                end
            end
        end

        
        ['Processing volume:' int2str(volnum) ' slice:' int2str(epi_slc_idx) ' Time:' int2str(time_idx)]
        
        if(sum(sum(epi_slc(:,:,find(epi_slc_idx==PInfo.slice_sequence(slc_idx),1)))) > Missing_threshold)
            % add the gaussian noise
            xtmp = x_particles + mvnrnd(zeros(6,1), PInfo.Gau_sigma, PInfo.num_of_particles)';
            
            % Evaluate the particles and find the optimum estimation
            tic;
            [pr_particles] = EvalParticles(@(x) Vol2SlcMetric(x, epi_slc_idx, epi_slc), xtmp);
            t=toc;
            ['Took ' num2str(t) ' seconds to evaluate the particles'];
            
            xt_meanest = sum(repmat(pr_particles(:)', [6,1]).*xtmp,2);
            tmp=xtmp-repmat(xt_meanest, [1 size(xtmp,2)]);
            covar_map=(tmp.*repmat(pr_particles(:).',[size(xtmp,1),1]))*tmp.';
            
            if(time_idx>2)
                xt_1 = xt_map(:, time_idx-1);
                xt_2 = xt_map(:, time_idx-2);
            end
            PInfo.Beta
            
            % Maximize the objective function
            OptimInfo = PInfo.optim_info;
            OptimInfo.rand_sigma = PInfo.Gau_sigma;
            run(PInfo.define_optimization_script);
            PInfo.optim_info = OptimInfo;
        else
            pr_particles = 1/PInfo.num_of_particles * ones(PInfo.num_of_particles,1);
            x_opt = xt_map(:,time_idx-1);
            xt_meanest = sum(repmat(pr_particles(:)', [6,1]).*xtmp,2);
            covar_map = PInfo.Gau_sigma;
        end
        
        %%%%%%%%%%%%%%%%%
        % Collecting the result
        xt_map(:, time_idx) = x_opt(:);
        xt_mean(:, time_idx) = xt_meanest;
        covar(:, time_idx)=diag(covar_map);
        f_value(time_idx) = -Vol2SlcMetric(x_opt(:), epi_slc_idx, epi_slc);
        
        %%% Resample the particles
        x_particles = mvnrnd(xt_map(:, time_idx), covar_map, PInfo.num_of_particles)';
        
        %%% Save the result
        save([PInfo.output_folder output_filename], 'time_idx', 'xt_map', 'xt_mean', 'f_value', 'covar');
    end
end

end