function [joint_origin, Gau_sigma]=EstimateModelParsMulti(init_pars, AnatVol, PInfo)

if(exist([PInfo.output_folder 'xt_firstround.mat'],'file'))
    load([PInfo.output_folder 'xt_firstround.mat']);
else
    EstimationLoopMulti(PInfo.parsest_volnum_start, PInfo.parsest_volnum_end, init_pars, AnatVol, PInfo, 'xt_firstround.mat');
end

load([PInfo.output_folder 'xt_firstround.mat']);
[xt_map_fit]=InterpPars(xt_map, PInfo.parsest_volnum_start, PInfo.parsest_volnum_end, PInfo);
['xt_map_fit size:' int2str(size(xt_map_fit))]

%%% Estimate the parameters
[joint_origin]=EstimateOrigin(xt_map_fit, PInfo, 0);
Vol2SlcInfo = PInfo.vol2slc_info;
Vol2SlcInfo.joint_origin = joint_origin;
xt_inv=CalcGlobalPars(xt_map_fit, Vol2SlcInfo, 1);
% ShowEstimatedPars(xt_inv);
dxt = xt_inv(:,2:end) - xt_inv(:,1:end-1);
Gau_sigma = diag(diag(cov(dxt')));

end