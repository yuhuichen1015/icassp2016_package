run('../Initial.m');
TEST_BEGIN;
%%
load([result_path 'MT_NMI064_FminMeanRandParsInitCtrlptsReg_LinearSysOffsetRealVol1to5_Multi0/PInfo.mat']);
load([result_path 'MT_NMI064_FminMeanRandParsInitCtrlptsReg_LinearSysOffsetRealVol1to5_Multi0/xt_map.mat']);

PInfo.missing_threshold = 0.15;
[xt_map_fit] = InterpPars(xt_map, 1, 5, PInfo);

load([PInfo.dataset_path 'TruthParsGlobal.mat']);

k=1:70;
Vol2SlcInfo = PInfo.vol2slc_info;

ShowEstimatedPars(xt_map(:,k), xt_global(:,k), Vol2SlcInfo);
RMSE=ShowParsRMSE(xt_map(:,k), xt_global(:,k), Vol2SlcInfo);
RMSE

ShowEstimatedPars(xt_map_fit(:,k), xt_global(:,k), Vol2SlcInfo);
RMSE=ShowParsRMSE(xt_map_fit(:,k), xt_global(:,k), Vol2SlcInfo);
RMSE

%%
TEST_END;