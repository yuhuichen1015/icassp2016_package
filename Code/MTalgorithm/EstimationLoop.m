function EstimationLoop(start_vol, end_vol, init_pars, AnatVol, PInfo, output_filename)

Vol2SlcMetric = PInfo.Vol2SlcMetric;
Vol2SlcInfo = PInfo.vol2slc_info;
% Set up the result data containers.
xt_map=zeros(6, (end_vol-start_vol+1)*numel(PInfo.slice_sequence));
xt_mean = zeros(6, (end_vol-start_vol+1)*numel(PInfo.slice_sequence));
covar=zeros(6, (end_vol-start_vol+1)*numel(PInfo.slice_sequence));
f_value=zeros(1, (end_vol-start_vol+1)*numel(PInfo.slice_sequence));

% Prepare initial particles
x_particles = repmat(init_pars(:), [1 PInfo.num_of_particles]);
%%% Start the multimodal tracking process
for volnum=start_vol:end_vol
    load([PInfo.dataset_path 'vol_' num2str(volnum) '.mat']);
    % Loop through each slice
    for slc_idx=1:numel(PInfo.slice_sequence)
        epi_slc_idx = PInfo.slice_sequence(slc_idx);
        epi_slc = data(:,:,epi_slc_idx);
        time_idx = (volnum-start_vol)*numel(PInfo.slice_sequence) + slc_idx;
        ['Processing volume:' int2str(volnum) ' slice:' int2str(epi_slc_idx) ' Time:' int2str(time_idx)]
       
        
        % Check whether the epi slice contains enough information to be
        % used
        Missing_threshold = PInfo.missing_threshold;
        if(sum(epi_slc(:)>10)/numel(epi_slc) > Missing_threshold)
            shrink_factor = 1;
            redo_cnt = 0;
            avgdp_threshold = 10;
            while(true)
                % add the gaussian noise
                xtmp = x_particles + mvnrnd(zeros(6,1), PInfo.Gau_sigma*shrink_factor, PInfo.num_of_particles)';
                
                % Evaluate the particles and find the optimum estimation
                tic;
                [pr_particles] = EvalParticles(@(x) Vol2SlcMetric(x, epi_slc_idx, epi_slc), xtmp);
                t=toc;
                ['Took ' num2str(t) ' seconds to evaluate the particles'];
                
                xt_meanest = sum(repmat(pr_particles(:)', [6,1]).*xtmp,2);
                tmp=xtmp-repmat(xt_meanest, [1 size(xtmp,2)]);
                covar_map=(tmp.*repmat(pr_particles(:).',[size(xtmp,1),1]))*tmp.';
                
                % Maximize the objective function
                OptimInfo = PInfo.optim_info;
                OptimInfo.rand_sigma = PInfo.Gau_sigma;
                run(PInfo.define_optimization_script);
                PInfo.optim_info = OptimInfo;
                
                if(time_idx==1)
                    break;
                elseif(CalcAvgDisplacement(xt_map(:, time_idx-1), x_opt(:), epi_slc_idx, Vol2SlcInfo)<avgdp_threshold)
                    CalcAvgDisplacement(xt_map(:, time_idx-1), x_opt(:), epi_slc_idx, Vol2SlcInfo)
                    break;
                else
                    'Weird optimization result'
                    CalcAvgDisplacement(xt_map(:, time_idx-1), x_opt(:), epi_slc_idx, Vol2SlcInfo)
                    x_particles = mvnrnd(xt_meanest, covar_map*shrink_factor, PInfo.num_of_particles)';
                    shrink_factor = shrink_factor*0.8;
                    redo_cnt = redo_cnt + 1
                end
                
            end
        else % Missing observation
            pr_particles = 1/PInfo.num_of_particles * ones(PInfo.num_of_particles,1);
            x_opt = mean(xtmp,2);
            xt_meanest = sum(repmat(pr_particles(:)', [6,1]).*xtmp,2);
            covar_map = PInfo.Gau_sigma;
        end
        
        %%%%%%%%%%%%%%%%%
        % Collecting the result
        xt_map(:, time_idx) = x_opt(:);
        xt_mean(:, time_idx) = xt_meanest;
        covar(:, time_idx)=diag(covar_map);
        f_value(time_idx) = ObjFunc(x_opt(:), epi_slc_idx, epi_slc);
        
        %%% Resample the particles
        x_particles = mvnrnd(xt_map(:, time_idx), covar_map, PInfo.num_of_particles)';
        
        x_opt(:)
        xt_mean(:, time_idx)
        
        save([PInfo.output_folder output_filename], 'time_idx', 'xt_map', 'xt_mean', 'f_value', 'covar');
        
    end
end

end