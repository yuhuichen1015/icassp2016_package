function [xt_map_fit]=InterpPars(xt_map, start_vol, end_vol, PInfo)
    
missing_obs = zeros(1, size(xt_map,2));
for volnum=start_vol:end_vol
    load([PInfo.dataset_path 'vol_' num2str(volnum) '.mat']);
    % Loop through each slice
    for slc_idx=1:numel(PInfo.slice_sequence)
        epi_slc_idx = PInfo.slice_sequence(slc_idx);
        epi_slc = data(:,:,epi_slc_idx);
        time_idx = (volnum-start_vol)*numel(PInfo.slice_sequence) + slc_idx;
        ['Processing volume:' int2str(volnum) ' slice:' int2str(epi_slc_idx) ' Time:' int2str(time_idx)]
       
        % Check whether the epi slice contains enough information to be
        % used
        Missing_threshold = PInfo.missing_threshold;
        if(sum(epi_slc(:)>10)/numel(epi_slc) <= Missing_threshold)
            missing_obs(time_idx) = 1;
        end
    end
end

[Nd, Nt] = size(xt_map);
xt_map_fit = zeros(Nd, Nt);

for d=1:Nd
    for t=1:Nt
        if(missing_obs(t)==1)
            t_dist = [-5:4];
            t_sel = t + t_dist;
            valid_idx = (t_sel>=1 & t_sel<=Nt);
            valid_idx(valid_idx) = missing_obs(t_sel(valid_idx))==0;
            t_sel = t_sel(valid_idx);
            t_dist = abs(t_dist(valid_idx));
            [yy, dd] = sort(t_dist, 'ascend');
            
            t_final = t_sel(dd(1:min(numel(dd), 5)));
            [func]=fit(t_final(:)-t, xt_map(d, t_final)', 'poly2');
            xt_map_fit(d,t) = func(0);
        else
            xt_map_fit(d,t) = xt_map(d,t);
        end
    end
end

end