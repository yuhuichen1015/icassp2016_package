function [pr_particles]=EvalParticles(Likelihood, xtmp)

%%%%%%%%%%%%%%%%%%%%%%%
[dim, Num_of_particles] = size(xtmp);
pr=zeros(Num_of_particles,1);
tic;
parfor i=1:Num_of_particles
    %%% Model is here
    pr(i) = Likelihood(xtmp(:,i));
end
toc;

% Normalize the particle values
pr=(pr-min(pr(:)))/(max(pr(:))-min(pr(:)));

% Generate the multi variate gaussian histogram
X=mvnrnd(zeros(1,dim), eye(dim), 100000);
Y=mvnpdf(X, zeros(1,dim), eye(dim));
Y = (Y-min(Y(:))) / (max(Y(:))-min(Y(:)));
hnorm=hist(Y, linspace(0,1,1000));

pr_eq = histeq(pr, hnorm);
pr_particles = pr_eq / sum(pr_eq(:));
['Num_of_particles:' int2str(Num_of_particles)]

end