function [init_pars, init_slc] = EstimateInitPars(VolRef, PInfo)
% set up the metric    
MetricInfo = PInfo.metric_info;
run(PInfo.define_metric_script);

% Prepare the transformed slice coordinate
Vol2SlcInfo = PInfo.vol2slc_info;
Vol2SlcInfo = CalcCoordinateInfo(Vol2SlcInfo);
Vol2SlcMetric = @(xt, slc_idx, EPI_slc) Metric(Vol2SliceInterpolation(VolRef, slc_idx, xt(:), Vol2SlcInfo),...
    EPI_slc);

% Get the first slice
load([PInfo.dataset_path 'vol_' num2str(PInfo.volnum_start) '.mat']);

% Perform the v2v registration first
Vol2SlcInfo.v2v_num_of_trials = 10;
Vol2SlcInfo.v2v_rand_range = 5;
[V2VInit]=RegisterVol2Vol(VolRef, data, Vol2SlcInfo);

% Get the first slice
epi_slc_idx = 1;
epi_slc = data(:,:,epi_slc_idx);

% First round with larger search range
Num_of_trials = 10;
OptimInfo = struct();
OptimInfo.rand_range = 5;
OptimInfo.num_of_trials = 5;
OptimInfo.tolerance_func = 10^-5;
OptimInfo.max_iteration = 1000;
OptimInfo.space_dim = 9;

xt_firstR = zeros(6,Num_of_trials);
fval = zeros(Num_of_trials,1);
for fsttrial = 1:Num_of_trials 
    ['First round. Trial:' int2str(fsttrial)]
    xtmp = V2VInit;
    pr_particles = 1;
    run([PInfo.matlab_path 'Optimization/Define_Opt_Fmin_MeanRandParsInit_CtrlPts.m']);
    
    fval(fsttrial) = ObjFunc(x_opt(:), epi_slc_idx, epi_slc);
    xt_firstR(:,fsttrial) = x_opt(:);
end

% The second round registration
[yy, idx] = sort(fval, 'ascend');
Num_of_trials = 3;
OptimInfo.rand_range = 3;
xt_secondR = zeros(6,Num_of_trials);
fval_second = zeros(Num_of_trials,1);
for sndtrial = 1:Num_of_trials
    ['Second round. Trial:' int2str(sndtrial)]
    xtmp = xt_firstR(:,idx(sndtrial));
    pr_particles = 1;
    run([PInfo.matlab_path 'Optimization/Define_Opt_Fmin_MeanRandParsInit_CtrlPts.m']);

    fval_second(sndtrial) = ObjFunc(x_opt(:), epi_slc_idx, epi_slc);
    xt_secondR(:,sndtrial) = x_opt(:);
end
[yy, idx] = sort(fval_second, 'ascend');
init_pars = xt_secondR(:,idx(1));
init_slc=Vol2SliceInterpolation(VolRef, epi_slc_idx, init_pars, Vol2SlcInfo);

end