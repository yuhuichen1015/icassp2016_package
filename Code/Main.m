%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generate figures for simulated data %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Initial;
AvgDisplacement_figure;
SimDataEstimation_figures;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Generate figures for simulated data %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
RealDataEstimation_figures;


%%
% Notice that it requires around 24 hours to finish the whole experiment.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Experiment for simulated data %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;
Initial;
parpool(4);
% Execute the volume to volume registration.
run('./ExecScripts/VolumeToVolumeRegistration_SimulatedExp.m');

% Execute the map slice to volume.
run('./ExecScripts/MappingSliceToVolume_SimulatedExp.m');

% Execute the HMT Algorithm.
run('./ExecScripts/HeadMotionTrackingMulti_SimulatedExp.m');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Experiment for real data %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear;
Initial;
parpool(4);
% Execute the volume to volume registration.
run('./ExecScripts/VolumeToVolumeRegistration_RunExp.m');

% Execute the map slice to volume.
run('./ExecScripts/MappingSliceToVolume_RunExp.m');

% Execute the HMT Algorithm.
run('./ExecScripts/HeadMotionTrackingMulti_RunExp.m');