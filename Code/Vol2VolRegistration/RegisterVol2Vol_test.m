run('../Initial.m');
TEST_BEGIN;
%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth.mat']);
% Load and add datainfo
% Add Data description:
% Load the activation volumns.
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
Vol2SlcInfo = struct();
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;
Vol2SlcInfo.jmin = DataInfo.jmin;
Vol2SlcInfo.jmax = DataInfo.jmax;
Vol2SlcInfo.dim_slices = DataInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = DataInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = DataInfo.max_ext_slices;
slices_sequence=[Vol2SlcInfo.jmin:2:Vol2SlcInfo.jmax...
    Vol2SlcInfo.jmin+1:2:Vol2SlcInfo.jmax];

run([matlab_path 'DataInfo/DataInfo_Origin.m']);
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

% Generate the moved volume
load([data_path 'AnatData/SimT2Vol.mat']);
TruePars = [0; 0; 0; 5; -7; 9;]
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
Vol2SlcInfo.v2v_num_of_trials = 5;
Vol2SlcInfo.v2v_rand_range = 5;
Vol_slices=Vol2SliceInterpolation(AnatVol, 1:Vol2SlcInfo.dim_slices(3), TruePars, Vol2SlcInfo);

figure; imagesc(Vol_slices(:,:,1));
figure; imagesc(Vol_slices(:,:,3));

load([data_path 'AnatData/SimT1Vol.mat']);
[EstPars]=RegisterVol2Vol(AnatVol, Vol_slices, Vol2SlcInfo);
EstPars


% Generate the moved volume
load([data_path 'AnatData/SimT2Vol.mat']);
TruePars = [3; 5; -5; 5; -3; -5;]
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
Vol_slices=Vol2SliceInterpolation(AnatVol, 1:Vol2SlcInfo.dim_slices(3), TruePars, Vol2SlcInfo);

figure; imagesc(Vol_slices(:,:,1));
figure; imagesc(Vol_slices(:,:,3));

load([data_path 'AnatData/SimT1Vol.mat']);
[EstPars]=RegisterVol2Vol(AnatVol, Vol_slices, Vol2SlcInfo);
EstPars

%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth.mat']);
% Load and add datainfo
% Add Data description:
% Load the activation volumns.
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
Vol2SlcInfo = struct();
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;
Vol2SlcInfo.jmin = DataInfo.jmin;
Vol2SlcInfo.jmax = DataInfo.jmax;
Vol2SlcInfo.dim_slices = DataInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = DataInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = DataInfo.max_ext_slices;
slices_sequence=[Vol2SlcInfo.jmin:2:Vol2SlcInfo.jmax...
    Vol2SlcInfo.jmin+1:2:Vol2SlcInfo.jmax];

run([matlab_path 'DataInfo/DataInfo_Origin.m']);
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

% Generate the moved volume
load([data_path 'AnatData/SimT2Vol.mat']);
Vol2SlcInfo.sysoffset_xyz = [5; -3; -5;];
Vol2SlcInfo.sysoffset_angles = [3; 5; -5;];
Vol2SlcInfo.sysoffset_dcm = EA2DCM(Vol2SlcInfo.sysoffset_angles);
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
Vol2SlcInfo.v2v_num_of_trials = 5;
Vol2SlcInfo.v2v_rand_range = 5;

Vol_slices=Vol2SliceInterpolation(AnatVol, 1:Vol2SlcInfo.dim_slices(3), zeros(6,1), Vol2SlcInfo);
figure; imagesc(Vol_slices(:,:,1));

% Estimate the system offset, reset the system offset
Vol2SlcInfo.sysoffset_xyz = [0 0 0];
Vol2SlcInfo.sysoffset_angles = [0 0 0];
Vol2SlcInfo.sysoffset_dcm = eye(3);
[EstPars]=RegisterVol2Vol(AnatVol, Vol_slices, Vol2SlcInfo);
EstPars


%%
TEST_END;