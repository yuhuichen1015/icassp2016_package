function [sysoffset_pars] = EstimateSysOffsetPars(VolRef, PInfo)
    
Vol2SlcInfo = PInfo.vol2slc_info;
% Prepare the transformed slice coordinate
line_d1 = linspace(Vol2SlcInfo.min_ext_slices(1),Vol2SlcInfo.max_ext_slices(1),Vol2SlcInfo.dim_slices(1));
line_d2 = linspace(Vol2SlcInfo.min_ext_slices(2),Vol2SlcInfo.max_ext_slices(2),Vol2SlcInfo.dim_slices(2));
[line_2, line_1] = meshgrid(line_d2,line_d1);
Vol2SlcInfo.line_1 = line_1;
Vol2SlcInfo.line_2 = line_2;
Vol2SlcInfo.slice_z_values = linspace(Vol2SlcInfo.min_ext_slices(3),Vol2SlcInfo.max_ext_slices(3),Vol2SlcInfo.dim_slices(3));

Vol2SlcInfo.delta1 = (Vol2SlcInfo.max_ext(1)-Vol2SlcInfo.min_ext(1)) / (Vol2SlcInfo.dim(1)-1);
Vol2SlcInfo.delta2 = (Vol2SlcInfo.max_ext(2)-Vol2SlcInfo.min_ext(2)) / (Vol2SlcInfo.dim(2)-1);
Vol2SlcInfo.delta3 = (Vol2SlcInfo.max_ext(3)-Vol2SlcInfo.min_ext(3)) / (Vol2SlcInfo.dim(3)-1);

% Prepare the volume slices
Vol_slices = zeros(Vol2SlcInfo.dim_slices);
for volnum = PInfo.volnum_start:PInfo.volnum_end
    load([PInfo.dataset_path 'vol_' num2str(volnum) '.mat']);
    Vol_slices = Vol_slices + data;
end
Vol_slices = Vol_slices / (PInfo.volnum_end-PInfo.volnum_start+1);

% Define the similarity metric
bin = 64;
edges_X = linspace(min(VolRef(:)), max(VolRef(:)), bin);
edges_Y = linspace(min(Vol_slices(:)), max(Vol_slices(:)), bin);
Metric = @(X,Y) Metric_MutualInformation(X, Y, edges_X, edges_Y);
Vol2VolMetric = @(xt) Metric(Vol2SliceInterpolation(VolRef, 1:Vol2SlcInfo.dim_slices(3), xt(:), Vol2SlcInfo),...
    Vol_slices);

Num_of_trials = Vol2SlcInfo.v2v_num_of_trials;
%Find the minimum MI
options = optimset;
options.MaxIter = 1000;
options.TolFun = 1e-4;
%%%%%%%%%%%%%%%%%%%%%%%
xopt_cand = zeros(6, Num_of_trials);
fval_cand = zeros(1, Num_of_trials);
['start to registering...']
for trial = 1:Num_of_trials
    xtmp = repmat(zeros(6,1), [1 7]) + Vol2SlcInfo.v2v_rand_range*randn(6,7);
    options.n_init = xtmp;
    [x_opt, f, exit_flag, output] = fminsearch_modified(@(x) -Vol2VolMetric(x), zeros(6,1), options);
    
    xopt_cand(:,trial) = x_opt(:);
    fval_cand(trial) = -Vol2VolMetric(x_opt(:));
end
%%% Choose the best one
[yy, ind] = sort(fval_cand, 'ascend');
sysoffset_pars = xopt_cand(:,ind(1));

end