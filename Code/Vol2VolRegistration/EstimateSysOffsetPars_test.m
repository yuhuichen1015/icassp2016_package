run('../Initial.m');
TEST_BEGIN;
%%
clear;
run('../Initial.m');
% Load the ground truth parameters
load([data_path 'SimulatedData/GroundTruth.mat']);
% Load and add datainfo
% Add Data description:
% Load the activation volumns.
run([matlab_path 'DataInfo/DataInfo_Sim.m']);
Vol2SlcInfo = struct();
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;
Vol2SlcInfo.jmin = DataInfo.jmin;
Vol2SlcInfo.jmax = DataInfo.jmax;
Vol2SlcInfo.dim_slices = DataInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = DataInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = DataInfo.max_ext_slices;
slices_sequence=[Vol2SlcInfo.jmin:2:Vol2SlcInfo.jmax...
    Vol2SlcInfo.jmin+1:2:Vol2SlcInfo.jmax];

run([matlab_path 'DataInfo/DataInfo_Origin.m']);
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
Vol2SlcInfo.v2v_num_of_trials = 1;
Vol2SlcInfo.v2v_rand_range = 5;

PInfo = struct();
PInfo.dataset_path = [data_path 'SimulatedData/LinearSysOffset/'];
PInfo.volnum_start = 1;
PInfo.volnum_end = 10;
PInfo.vol2slc_info = Vol2SlcInfo;

load([data_path 'AnatData/SimT1Vol.mat']);

EstPars = EstimateSysOffsetPars(AnatVol, PInfo);
EstPars

%%
PInfo.volnum_start = 1;
PInfo.volnum_end = 100;

EstPars = EstimateSysOffsetPars(AnatVol, PInfo);
EstPars

%% Test for the run dataset
clear;
run('../Initial.m');
% Load the ground truth parameters
run([matlab_path 'DataInfo/DataInfo_RunAnat.m']);
Vol2SlcInfo = struct();
Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

load([data_path 'RealData/Run1/GenerationInfo.mat']);
Vol2SlcInfo.jmin = GenerationInfo.jmin;
Vol2SlcInfo.jmax = GenerationInfo.jmax;
Vol2SlcInfo.dim_slices = GenerationInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = GenerationInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = GenerationInfo.max_ext_slices;
slices_sequence=GenerationInfo.slice_sequence;
Vol2SlcInfo.interp_order = 1;
Vol2SlcInfo.account_thickness = true;
Vol2SlcInfo.v2v_num_of_trials = 1;
Vol2SlcInfo.v2v_rand_range = 5;

PInfo = struct();
PInfo.dataset_path = [data_path 'RealData/Run1/'];
PInfo.volnum_start = 1;
PInfo.volnum_end = 126;
PInfo.vol2slc_info = Vol2SlcInfo;

load([data_path 'AnatData/RunT1Vol.mat']);

% Before
idx = 2;
ObservedSlices=Vol2SliceInterpolation(AnatVol, idx, zeros(6,1), Vol2SlcInfo);
figure; imagesc(ObservedSlices), axis image;
load([PInfo.dataset_path 'vol_1.mat']);
figure; imagesc(data(:,:,idx)), axis image;

EstPars = EstimateSysOffsetPars(AnatVol, PInfo);
EstPars

% Examine
ObservedSlices=Vol2SliceInterpolation(AnatVol, idx, EstPars, Vol2SlcInfo);
figure; imagesc(ObservedSlices), axis image;
load([PInfo.dataset_path 'vol_1.mat']);
figure; imagesc(data(:,:,idx)), axis image;


%%
TEST_END;