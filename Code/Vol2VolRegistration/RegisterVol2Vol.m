function [Pars]=RegisterVol2Vol(VolRef, Vol_slices, DataInfo)
    
% Prepare the transformed slice coordinate
line_d1 = linspace(DataInfo.min_ext_slices(1),DataInfo.max_ext_slices(1),DataInfo.dim_slices(1));
line_d2 = linspace(DataInfo.min_ext_slices(2),DataInfo.max_ext_slices(2),DataInfo.dim_slices(2));
[line_2, line_1] = meshgrid(line_d2,line_d1);
DataInfo.line_1 = line_1;
DataInfo.line_2 = line_2;
DataInfo.slice_z_values = linspace(DataInfo.min_ext_slices(3),DataInfo.max_ext_slices(3),DataInfo.dim_slices(3));

DataInfo.delta1 = (DataInfo.max_ext(1)-DataInfo.min_ext(1)) / (DataInfo.dim(1)-1);
DataInfo.delta2 = (DataInfo.max_ext(2)-DataInfo.min_ext(2)) / (DataInfo.dim(2)-1);
DataInfo.delta3 = (DataInfo.max_ext(3)-DataInfo.min_ext(3)) / (DataInfo.dim(3)-1);

% Define the similarity metric
bin = 64;
edges_X = linspace(min(Vol_slices(:)), max(Vol_slices(:)), bin);
edges_Y = linspace(min(VolRef(:)), max(VolRef(:)), bin);
Metric = @(X,Y) Metric_MutualInformation(X, Y, edges_X, edges_Y);
Vol2VolMetric = @(xt) Metric(Vol_slices,...
    Vol2SliceInterpolation(VolRef, 1:DataInfo.dim_slices(3), xt(:), DataInfo));

Num_of_trials = DataInfo.v2v_num_of_trials;
%Find the minimum MI
options = optimset;
options.MaxIter = 1000;
options.TolFun = 1e-4;
%%%%%%%%%%%%%%%%%%%%%%%
xopt_cand = zeros(6, Num_of_trials);
fval_cand = zeros(1, Num_of_trials);
for trial = 1:Num_of_trials
    xtmp = repmat(zeros(6,1), [1 7]) + DataInfo.v2v_rand_range*randn(6,7);
    options.n_init = xtmp;
    [x_opt, f, exit_flag, output] = fminsearch_modified(@(x) -Vol2VolMetric(x), zeros(6,1), options);
    
    xopt_cand(:,trial) = x_opt(:);
    fval_cand(trial) = -Vol2VolMetric(x_opt(:));
end
%%% Choose the best one
[yy, ind] = sort(fval_cand, 'ascend');
Pars = xopt_cand(:,ind(1));

end