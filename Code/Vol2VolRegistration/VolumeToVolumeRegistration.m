
% Set up input EPI data
input_folder = PInfo.dataset_path;
load([input_folder 'GenerationInfo.mat']);
dim_slices = GenerationInfo.dim_slices;
min_ext_slices = GenerationInfo.min_ext_slices;
max_ext_slices = GenerationInfo.max_ext_slices;
slice_Z_values=linspace(min_ext_slices(3),max_ext_slices(3),dim_slices(3));
slice_sequence = GenerationInfo.slice_sequence;

Vol2SlcInfo = PInfo.vol2slc_info;
Vol2SlcInfo.dim_slices = GenerationInfo.dim_slices;
Vol2SlcInfo.min_ext_slices = GenerationInfo.min_ext_slices;
Vol2SlcInfo.max_ext_slices = GenerationInfo.max_ext_slices;

% Set up reference data
load(PInfo.dataset_anat);
run(PInfo.dataset_anatinfo);

Vol2SlcInfo.dim = DataInfo.dim;
Vol2SlcInfo.min_ext = DataInfo.min_ext;
Vol2SlcInfo.max_ext = DataInfo.max_ext;

% Estimate the system offset
Vol2SlcInfo.v2v_num_of_trials = 10;
Vol2SlcInfo.v2v_rand_range = 5;
PInfo.vol2slc_info = Vol2SlcInfo;
tic;
[sysoffset_pars] = EstimateSysOffsetPars(AnatVol, PInfo);
t=toc;
['Took ' num2str(t) ' seconds to estimate the system offset parameters.']

% Update the vol2slc info
Vol2SlcInfo.sysoffset_angles = sysoffset_pars(1:3);
Vol2SlcInfo.sysoffset_xyz = sysoffset_pars(4:6);
Vol2SlcInfo.sysoffset_dcm = EA2DCM(Vol2SlcInfo.sysoffset_angles);
Vol2SlcInfo.v2v_num_of_trials = PInfo.v2v_num_of_trials;
Vol2SlcInfo.v2v_rand_range = PInfo.v2v_rand_range;
PInfo.vol2slc_info = Vol2SlcInfo;

% Create result folder
mkdir(PInfo.output_folder);
isexist = false; %exist([PInfo.output_folder 'xt_volume.mat'], 'file');
if(isexist && PInfo.overwrite==0)
    error('Result file exists. Not overwriting. If you really want to '...
        + 'execute the exeperiment, please set PInfo.overwrite to be true.');
elseif(isexist && PInfo.overwrite==2)
    warning('Result file exists. Continuing previous status...');
else
    % Save the PInfo
    PInfo
    xt_volume = zeros(6, PInfo.volnum_end-PInfo.volnum_start+1);
    xt_map = zeros(6, (PInfo.volnum_end-PInfo.volnum_start+1)*dim_slices(3));
end
save([PInfo.output_folder 'PInfo.mat'], 'PInfo');

%%% Start the multimodal tracking process
for volnum=PInfo.volnum_start:PInfo.volnum_end
    load([input_folder 'vol_' num2str(volnum) '.mat']);
    
    % Registering the volume
    ['Estimating ' int2str(volnum) 'st volume...']
    tic;
    [pars]=RegisterVol2Vol(AnatVol, data, Vol2SlcInfo);
    t=toc;
    ['Took ' num2str(t) ' seconds to register this volume']
    
    % Save the estimated parameters.
    xt_volume(:,volnum) = pars(:);
    xt_map(:, (volnum-1)*dim_slices(3)+1:volnum*dim_slices(3)) = repmat(pars(:), [1 dim_slices(3)]);
    
    save([PInfo.output_folder 'xt_map.mat'], 'volnum', 'xt_map');
    save([PInfo.output_folder 'xt_volume.mat'], 'volnum', 'xt_volume');
end
